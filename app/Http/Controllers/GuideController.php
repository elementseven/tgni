<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Region;
use App\Models\Language;
use App\Models\TourType;
use \stdClass;
use DB;

class GuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('guides.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {

        if ($request->session()->has('session_rand')) {
            if((time() - $request->session()->get('session_rand')) > 3600){
                $request->session()->put('session_rand', time());
            }
        }else{
            $request->session()->put('session_rand', time());
        }

        $region = $request->input('region');
        $type = $request->input('type');
        $lang = $request->input('lang');
        $search = $request->input('search');

        $guides = Profile::where('status', 1)
        ->when($search != "" && $search != null && $search != "null", function($q) use ($search) {
            $q->where('name','LIKE', '%'.$search.'%');
        })
        ->whereHas('regions', function($q) use($region){
            if($region != '*'){
              $q->where('slug', $region);
            }
        })
        ->whereHas('tourTypes', function($q) use($type){
            if($type != '*'){
              $q->where('slug', $type);
            }
        })
        ->whereHas('languages', function($q) use($lang){
            if($lang != '*'){
              $q->where('slug', $lang);
            }
        })
        ->whereHas('media', function($q){
            $q->where('collection_name', 'profiles');
        })
        ->with('tourExperiences','languages','regions')
        ->orderBy(DB::raw('RAND('.$request->session()->get('session_rand').')'))
        ->paginate($request->input('limit'));
        
        foreach($guides as $g){
            $g->normal = $g->getFirstMediaUrl('profiles','normal');
            $g->webp = $g->getFirstMediaUrl('profiles','normal-webp');
            $g->mime = $g->getFirstMedia('profiles')->mime_type;
            foreach($g->languages as $lang){
                $lang->image = $lang->getFirstMediaUrl('languages');
            }
        }

        return $guides;

    }

    // Get images
    public function getGuideImages(Profile $profile){

        $images = $profile->guideImages;
        foreach($images as $g){
            $g->normal = $g->getFirstMediaUrl('guideimages','normal');
            $g->webp = $g->getFirstMediaUrl('guideimages','normal-webp');
            $g->mime = $g->getFirstMedia('guideimages')->mime_type;
        }
        return $images;
    }

    // Get filter options
    public function getFilterOptions(){

        $regions = Region::orderBy('name', 'asc')->get();
        $langauages = Language::orderBy('name', 'asc')->whereHas('profiles')->get();
        $tourtypes = TourType::orderBy('name', 'asc')->whereHas('profiles')->get();


        return response()->json([$regions, $langauages, $tourtypes]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile, $slug)
    {
        $pattern = "/<p[^>]*>&nbsp;<\\/p[^>]*>/"; 
        $profile->description = preg_replace($pattern, '', $profile->description); 
        return view('guides.show')->with(['profile' => $profile]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
