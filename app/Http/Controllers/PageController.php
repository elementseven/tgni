<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function tourGuideLogin(){
        return view('auth.login');
    }
    public function about(){
        return view('about');
    }
    public function join(){
        return view('join');
    }
    public function tandcs(){
        return view('tandcs');
    }
    public function privacyPolicy(){
        return view('privacyPolicy');
    }
    public function contact(){
        return view('contact');
    }
    public function regions(){
        return view('regions.index');
    }
    public function regionsCauseway(){
        return view('regions.causeway');
    }
    public function regionsBelfastCity(){
        return view('regions.belfast');
    }
    public function regionsFermanagh(){
        return view('regions.fermanagh');
    }
    public function regionsNewry(){
        return view('regions.newry');
    }
    public function regionsDerry(){
        return view('regions.derry');
    }
    public function regionsNewtownabbey(){
        return view('regions.newtownabbey');
    }
    public function regionsAntrim(){
        return view('regions.antrim');
    }
    public function regionsArds(){
        return view('regions.ards');
    }
    public function regionsLisburn(){
        return view('regions.lisburn');
    }
    public function regionsArmagh(){
        return view('regions.armagh');
    }
    public function regionsMidUlster(){
        return view('regions.mid-ulster');
    }
}
