<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Profile;

use Newsletter;
use Mail;
use Auth;

class SendMail extends Controller
{
    public function enquiry(Request $request){

        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 

        $subject = "General Enquiry";
        $name = $request->input('name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.enquiry',[
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $name, $content, $phone){
            $message->from('donotreply@tourguidesni.com', 'Tour Guides NI');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('info@tourguidesni.com');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

    public function sendJoinForm(Request $request){

        // Validate the form data
        $this->validate($request,[
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 
        
        $subject = "Join Enquiry";
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $content = $request->input('message');
        $phone = null;
        if($request->input('phone') != ""){
            $phone = $request->input('phone');
        }
        Mail::send('emails.joinEnquiry',[
            'first_name' => $first_name,
            'last_name' => $last_name,
            'subject' => $subject,
            'email' => $email,
            'phone' => $phone,
            'content' => $content
        ], function ($message) use ($subject, $email, $first_name, $last_name, $content, $phone){
            $message->from('donotreply@tourguidesni.com', 'Tour Guides NI');
            $message->subject($subject);
            $message->replyTo($email);
            $message->to('info@tourguidesni.com');
            //$message->to('luke@elementseven.co');
        });
        return 'success';
    }

    public function sendBookingRequest(Request $request){
        // Validate the form data
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'date' => 'required',
            'email' => 'required|email',
            'message' => 'required|string',
        ]); 
        $guide = Profile::where('id', $request->input('guideid'))->first();
        if($guide){
            $subject = "Booking Enquiry";
            $name = $request->input('name');
            $date = $request->input('date');
            $email = $request->input('email');
            $content = $request->input('message');
            $phone = null;
            if($request->input('phone') != ""){
                $phone = $request->input('phone');
            }
            Mail::send('emails.bookingEnquiry',[
                'name' => $name,
                'date' => $date,
                'subject' => $subject,
                'guide' => $guide,
                'email' => $email,
                'phone' => $phone,
                'content' => $content
            ], function ($message) use ($subject, $guide, $email, $name, $date, $content, $phone){
                $message->from('donotreply@tourguidesni.com', 'Tour Guides NI');
                $message->subject($subject);
                $message->replyTo($email);
                $message->to('info@tourguidesni.com');
                //$message->to('luke@elementseven.co');
            });

            if($guide->email != null){
                Mail::send('emails.guideBookingEnquiry',[
                    'name' => $name,
                    'date' => $date,
                    'subject' => $subject,
                    'guide' => $guide,
                    'email' => $email,
                    'phone' => $phone,
                    'content' => $content
                ], function ($message) use ($subject, $guide, $email, $name, $date, $content, $phone){
                    $message->from('donotreply@tourguidesni.com', 'Tour Guides NI');
                    $message->subject($subject);
                    $message->replyTo($email);
                    $message->to($guide->email);
                    //$message->to('luke@elementseven.co');
                });
            }
            return 'success';
        }else{
            $data = array(
                'code'      => 400,
                'message'   => 'error_occured',
                'data'      => $request->input('guide') . " is the guide id"
            );
            return response()->json($data, 400);
        }
        
    }

    public function mailingListSignup(Request $request){
        $this->validate($request,[
          'email' => 'required|string|email|max:255'
        ]);
        Newsletter::subscribe($request->input('email'));
        return "success";
    }
}
