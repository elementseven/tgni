<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class GuideMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user() != NULL && $request->user()->role->id == 1){
            return $next($request);
        }else if($request->user() != NULL && $request->user()->role->id == 2){
            return $next($request);
        }else{
            Auth::logout();
            return redirect('login');
        }
    }
}
