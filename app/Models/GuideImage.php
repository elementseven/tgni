<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class GuideImage extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'image','profile_id','order'
    ];
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(400);
        $this->addMediaConversion('normal-webp')->width(400)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(1600);
        $this->addMediaConversion('double-webp')->width(1600)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 400, 400);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 800, 450);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 800, 450)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('guideimages')->singleFile();
    }

    public function profile(){
        return $this->belongsTo('App\Models\Profile');
    }
}