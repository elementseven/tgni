<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Language extends  Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name','slug','image','order'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($language) {
            $language->slug = Str::slug($language->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('languages')->singleFile();
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'language_profile')->select('order')->withPivot('profile_id','order');
    }
}

