<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class NewsDocument extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name','slug','file','post_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($newsdocument) {
            $newsdocument->slug = Str::slug($newsdocument->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('newsdocuments')->singleFile();
    }

    public function post(){
        return $this->belongsTo('App\Models\Post');
    }
}
