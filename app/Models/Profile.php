<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Profile extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
    protected $fillable = [
        'name',
        'slug',
        'phone',
        'email',
        'website',
        'address',
        'description',
        'qualifications',
        'instagram',
        'facebook',
        'twitter',
        'youtube',
        'linkedin',
        'image',
        'user_id',
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($profile) {
            $profile->slug = Str::slug($profile->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(325);
        $this->addMediaConversion('normal-webp')->width(325)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(650);
        $this->addMediaConversion('double-webp')->width(650)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 400, 400);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 650, 932);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 650, 932)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('profiles')->singleFile();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function regions(){
        return $this->belongsToMany('App\Models\Region', 'profile_region')->select('order','name','slug')->withPivot('region_id','order');
    }
    public function languages(){
        return $this->belongsToMany('App\Models\Language', 'language_profile')->select('id','order','image','name')->withPivot('language_id','order');
    }
    public function tourTypes(){
        return $this->belongsToMany('App\Models\TourType', 'profile_tour_type')->select('order')->withPivot('tour_type_id','order');
    }
    public function tourExperiences(){
        return $this->belongsToMany('App\Models\TourExperience', 'profile_tour_experience')->select('order','name')->withPivot('tour_experience_id','order');
    }
    public function tours(){
        return $this->hasMany('App\Models\Tour');
    }
    public function guideImages(){
        return $this->hasMany('App\Models\GuideImage');
    }
}
