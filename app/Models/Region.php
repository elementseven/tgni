<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Region extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'name','slug','image','order'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($region) {
            $region->slug = Str::slug($region->name, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->keepOriginalImageFormat()->width(235);
        $this->addMediaConversion('normal-webp')->width(235)->format('webp');
        $this->addMediaConversion('double')->keepOriginalImageFormat()->width(470);
        $this->addMediaConversion('double-webp')->width(470)->format('webp');
        $this->addMediaConversion('thumbnail')->keepOriginalImageFormat()->crop('crop-center', 400, 400);
        $this->addMediaConversion('featured')->keepOriginalImageFormat()->crop('crop-center', 470, 634);
        $this->addMediaConversion('featured-webp')->crop('crop-center', 470, 634)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('regions')->singleFile();
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_region')->select('order')->withPivot('profile_id','order');
    }
}
