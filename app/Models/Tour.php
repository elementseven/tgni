<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tour extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','slug','link','order','profile_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($tour) {
            $tour->slug = Str::slug($tour->name, "-");
        });
    }

    public function profile(){
        return $this->belongsTo('App\Models\Profile');
    }
}
