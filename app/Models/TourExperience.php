<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TourExperience extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','slug','order'
    ];
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($tourexperience) {
            $tourexperience->slug = Str::slug($tourexperience->name, "-");
        });
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_tour_experience')->select('order')->withPivot('profile_id','order');
    }
}
