<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TourType extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','slug','order'
    ];
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($tourtype) {
            $tourtype->slug = Str::slug($tourtype->name, "-");
        });
    }

    public function profiles(){
        return $this->belongsToMany('App\Models\Profile', 'profile_tour_type')->select('order')->withPivot('profile_id','order');
    }
}
