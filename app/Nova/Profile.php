<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Froala\NovaFroalaField\Froala;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Profile extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Profile::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Name')->sortable(),
            Boolean::make('Show on website','status')->sortable(),
            Text::make('Phone')->sortable(),
            Text::make('Email')->sortable(),
            Text::make('Website')->hideFromIndex(),
            Text::make('Address')->hideFromIndex(),
            Text::make('Qualifications')->hideFromIndex(),
            Text::make('Facebook')->hideFromIndex(),
            Text::make('Instagram')->hideFromIndex(),
            Text::make('Twitter')->hideFromIndex(),
            Text::make('Youtube')->hideFromIndex(),
            Text::make('Linkedin')->hideFromIndex(),
            Date::make('Date Added', 'created_at')->hideFromIndex()->hideWhenCreating()->hideWhenUpdating(),
            Image::make('Profile Image', 'image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('profiles');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('profiles', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('profiles', 'thumbnail');
            })->deletable(false),
            
            
            Froala::make('Description')->withFiles('public'),

            BelongsTo::make('User'),
            BelongsToMany::make('Languages')
            ->fields(function () {
                return [
                    Number::make('Order'),
                ];
            }),
            BelongsToMany::make('Regions')
            ->fields(function () {
                return [
                    Number::make('Order'),
                ];
            }),
            BelongsToMany::make('Tour Types','tourTypes')
            ->fields(function () {
                return [
                    Number::make('Order'),
                ];
            }),
            BelongsToMany::make('Tour Experiences','tourExperiences')
            ->fields(function () {
                return [
                    Number::make('Order'),
                ];
            }),
            HasMany::make('Guide Images','GuideImages'),
            HasMany::make('Tours'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
