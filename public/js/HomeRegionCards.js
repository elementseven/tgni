(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["HomeRegionCards"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeRegionCardsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()},

data:function data(){
return {
errors:{},
success:false,
loaded:true};

},
methods:{}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeRegionCardsVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,"#home-region-cards {\n  position: absolute;\n  bottom: 0;\n  right: 0;\n  width: 60%;\n  padding-top: 2rem;\n  padding-left: 0;\n  overflow: hidden;\n}\n#home-region-cards .home-region-card {\n    position: relative;\n    width: 260px;\n    height: 372px;\n    background-repeat: no-repeat;\n    background-position: center center;\n    background-size: 110%;\n    border-radius: 20px;\n    text-align: left;\n    overflow: hidden;\n    cursor: pointer;\n    padding-top: 2rem;\n    padding-left: 2rem;\n    box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.3) !important;\n    transition: all 300ms ease;\n}\n#home-region-cards .home-region-card a {\n      position: absolute;\n      top: 0;\n      left: 0;\n      width: 100%;\n      height: 100%;\n      display: block;\n}\n#home-region-cards .home-region-card .region-info {\n      position: absolute;\n      bottom: 0;\n      left: 0;\n      width: 100%;\n      padding: 7rem 1rem 1rem 1rem;\n      background: #111213;\n      background: linear-gradient(180deg, rgba(17, 18, 19, 0) 0%, #111213 100%);\n}\n#home-region-cards .home-region-card .region-info p {\n        color: #fff;\n}\n#home-region-cards .home-region-card .region-info p.region-card-top {\n          font-family: ff-zwo-corr-web-pro, sans-serif;\n          color: #fff;\n          font-size: 0.8rem;\n          line-height: 1;\n          margin-bottom: 0.3rem;\n          letter-spacing: 0.2rem;\n}\n#home-region-cards .home-region-card .region-info p.region-card-title {\n          font-family: ff-zwo-corr-web-pro, sans-serif;\n          color: #fff;\n          font-size: 1.2rem;\n          line-height: 1.1;\n          margin-bottom: 0;\n}\n#home-region-cards .home-region-card:hover {\n      background-size: 115%;\n}\n#home-region-cards .owl-carousel .owl-stage-outer {\n    overflow: visible !important;\n}\n#home-region-cards .owl-stage {\n    margin: auto;\n}\n#home-region-cards .owl-theme .owl-nav {\n    margin-top: 1rem;\n    margin-bottom: -35px;\n    text-align: left;\n}\n#home-region-cards .owl-theme .owl-nav .owl-prev {\n      margin-right: 520px;\n      font-size: 0;\n      position: relative;\n      color: #1D252D;\n      padding: 0;\n      width: 40px;\n      height: 40px;\n      border: 2px solid #fff;\n      border-radius: 5px;\n      background-color: transparent;\n}\n#home-region-cards .owl-theme .owl-nav .owl-prev:after {\n        position: absolute;\n        content: \"\";\n        right: 0;\n        top: 0;\n        background-image: url(\"/img/icons/chevron-left.svg\");\n        background-size: contain;\n        background-position: center;\n        width: 11px;\n        margin-right: 13px;\n        height: 36px;\n        background-repeat: no-repeat;\n}\n#home-region-cards .owl-theme .owl-nav .owl-next {\n      font-size: 0;\n      position: relative;\n      color: #1D252D;\n      padding: 0;\n      width: 40px;\n      height: 40px;\n      border: 2px solid #fff;\n      border-radius: 5px;\n      background-color: transparent;\n}\n#home-region-cards .owl-theme .owl-nav .owl-next:after {\n        position: absolute;\n        content: \"\";\n        left: 0;\n        top: 0;\n        background-image: url(\"/img/icons/chevron-right.svg\");\n        background-size: contain;\n        background-position: center;\n        width: 11px;\n        margin-left: 13px;\n        height: 36px;\n        background-repeat: no-repeat;\n}\n#home-region-cards .owl-theme .owl-dots {\n    margin: 0 62px;\n    width: 490px;\n}\n#home-region-cards .owl-theme .owl-dots .owl-dot span {\n      width: 30px;\n      height: 4px;\n      background: #fff;\n      border-radius: 0;\n}\n#home-region-cards .owl-theme .owl-dots .owl-dot.active span {\n      background: #F6CF3C;\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {\n#home-region-cards .owl-theme .owl-dots {\n    width: 220px;\n}\n#home-region-cards .owl-theme .owl-nav .owl-prev {\n    margin-right: 270px;\n}\n}\n@media only screen and (max-width: 767px) {\n#home-region-cards {\n    width: 80%;\n}\n#home-region-cards .owl-theme .owl-nav,\n    #home-region-cards .owl-theme .owl-dots {\n      display: none !important;\n}\n#home-region-cards .home-region-card {\n      padding-left: 1rem;\n      padding-right: 1rem;\n      width: 260px;\n      height: 200px;\n}\n#home-region-cards .home-region-card .region-info p.region-card-top {\n        font-size: 3vw;\n}\n#home-region-cards .home-region-card .region-info p.region-card-title {\n        font-size: 5vw;\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeRegionCardsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeRegionCards.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__.default,options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__.default.locals||{};

/***/},

/***/"./resources/js/components/Home/HomeRegionCards.vue":



function resourcesJsComponentsHomeHomeRegionCardsVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./HomeRegionCards.vue?vue&type=template&id=28d1f60b& */"./resources/js/components/Home/HomeRegionCards.vue?vue&type=template&id=28d1f60b&");
/* harmony import */var _HomeRegionCards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./HomeRegionCards.vue?vue&type=script&lang=js& */"./resources/js/components/Home/HomeRegionCards.vue?vue&type=script&lang=js&");
/* harmony import */var _HomeRegionCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./HomeRegionCards.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
_HomeRegionCards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__.render,
_HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Home/HomeRegionCards.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/HomeRegionCards.vue?vue&type=script&lang=js&":



function resourcesJsComponentsHomeHomeRegionCardsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeRegionCards.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&":



function resourcesJsComponentsHomeHomeRegionCardsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeRegionCards.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/Home/HomeRegionCards.vue?vue&type=template&id=28d1f60b&":



function resourcesJsComponentsHomeHomeRegionCardsVueVueTypeTemplateId28d1f60b(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeRegionCards_vue_vue_type_template_id_28d1f60b___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeRegionCards.vue?vue&type=template&id=28d1f60b& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=template&id=28d1f60b&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeRegionCards.vue?vue&type=template&id=28d1f60b&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeRegionCardsVueVueTypeTemplateId28d1f60b(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{staticClass:"container-fluid pb-5",attrs:{id:"home-region-cards"}},
[
_c("div",{staticClass:"row"},[
_c(
"div",
{staticClass:"col-12"},
[
_c(
"carousel",
{
attrs:{
items:4,
margin:15,
center:false,
autoHeight:true,
loop:true,
autoplay:true,
autoplayTimeout:7000,
autoplaySpeed:500,
autoWidth:true,
nav:true,
dots:true}},


[
_c("div",{staticClass:"home-region-card cards-causeway"},[
_c(
"a",
{attrs:{href:"/regions/causeway-coast-glens"}},
[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("GIANTS CAUSEWAY")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Causeway Coast"),
_c("br"),
_vm._v("& Glens")])])])]),





_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-belfast"},[
_c("a",{attrs:{href:"/regions/belfast-city"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("TITANIC BELFAST")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Belfast"),
_c("br"),
_vm._v("City")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-fermanagh"},[
_c("a",{attrs:{href:"/regions/fermanagh-omagh"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("STAIRWAY TO HEAVEN")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Fermanagh"),
_c("br"),
_vm._v("& Omagh")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-mournes"},[
_c("a",{attrs:{href:"/regions/newry-mourne-down"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("MOURNE MOUNTAINS")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Newry, Mourne"),
_c("br"),
_vm._v("& Down")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-derry"},[
_c("a",{attrs:{href:"/regions/derry-strabane"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("DERRY'S WALLS")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Derry City &"),
_c("br"),
_vm._v("Strabane")])])])]),




_vm._v(" "),
_c(
"div",
{staticClass:"home-region-card cards-newtownabbey"},
[
_c(
"a",
{attrs:{href:"/regions/antrim-newtownabbey"}},
[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("NATURE & CULTURE")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Antrim &"),
_c("br"),
_vm._v("Newtownabbey")])])])]),






_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-antrim"},[
_c("a",{attrs:{href:"/regions/mid-east-antrim"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("GLENS OF ANTRIM")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Mid & East"),
_c("br"),
_vm._v("Antrim")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-ards"},[
_c("a",{attrs:{href:"/regions/ards-north-down"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("SCRABO TOWER")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Ards &"),
_c("br"),
_vm._v("North Down")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-lisburn"},[
_c("a",{attrs:{href:"/regions/lisburn-castlereagh"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("CASTLE GARDENS")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Lisburn &"),
_c("br"),
_vm._v("Castlereagh")])])])]),




_vm._v(" "),
_c("div",{staticClass:"home-region-card cards-armagh"},[
_c(
"a",
{attrs:{href:"/regions/armagh-banbridge-craigavon"}},
[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("CHRISTIAN HERITAGE")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Armagh, Banbridge & Craigavon")])])])]),





_vm._v(" "),
_c(
"div",
{staticClass:"home-region-card cards-mid-ulster"},
[
_c("a",{attrs:{href:"/regions/mid-ulster"}},[
_c("div",{staticClass:"region-info"},[
_c("p",{staticClass:"region-card-top"},[
_vm._v("SEAMUS HEANEY")]),

_vm._v(" "),
_c("p",{staticClass:"region-card-title"},[
_vm._v("Mid-Ulster")])])])])])],








1)])]);




};
var _staticRenderFns=[];
_render._withStripped=true;



/***/}}]);

}());
