@php
$page = 'About';
$pagetitle = "About | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container position-relative py-5 mt-5 mob-mt-0">
            <div class="row pt-5 mt-5">
                <div class="col-lg-8  position-relative z-2">
                    <h1 class="mb-4">About TGNI</h1>
                    <p class="mb-4">Tour Guides NI is Northern Ireland’s tourist guiding body focusing on quality, training and continuous professional development of its guides. TGNI is spearheaded by a number of established ,experienced, professional guides elected annually from within its membership.</p>
                    <button class="btn btn-primary" type="button">View guides</button>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container-fluid py-5 position-relative">
    <div class="row py-5 mb-5">
        <div class="col-lg-6 pl-0 mob-px-0">
            <div class="left-img">
                <picture>
                    <source data-srcset="/img/regions/belfast/titanic-museum.webp" type="image/webp"/> 
                    <source data-srcset="/img/regions/belfast/titanic-museum.jpg" type="image/jpeg"/> 
                    <img data-src="/img/regions/belfast/events/titanic-museum.jpg" type="image/jpeg" alt="Belfast's Titanic Museum - TGNI" class="lazy w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                    <p class="mimic-h2 text-capitalize">Our Aim</p>
                    <p>TGNI’s key aim is to offer visitors to Northern Ireland a warm welcome and to show them the best we have to offer, inspiring, educating and entertaining them along the way. Our vision is to be an Association whose members are all professionally qualified and skilled to deliver consistently high-quality visitor experiences to tourists throughout the island of Ireland.</p>
                    <a href="/">
                        <button class="btn btn-primary">Book a guide</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mb-5">
                    <h2 class="mb-4">Our Guides</h2>
                    <p>TGNI has a broad range of expert Northern Ireland Tour Guides in its membership, ranging from Site Guides to City Guides and all Ireland Guides as reflected in the selection of tours we offer. Our knowledgeable and professional guides are passionate about our beautiful country and enthusiastic and entertaining in their way of delivery.<p/> 
                    <p class="mb-4">Tours can be delivered in a variety of ways and in a large range of languages: TGNI members can offer walking, taxi, driver/guides and coach tours; the field of subjects and interests is extensive. Tours can be tailored to meet clients’ specific needs; TGNI aims to help and guide and will be happy to discuss suggested itineraries.</p>
                    <a href="/">
                        <button class="btn btn-primary">Book a guide</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<desktop-regions class="d-none d-lg-block"></desktop-regions>
<mob-regions class="d-lg-none"></mob-regions>
<seen-enough :link="'/contact'" :btntext="'Get in touch'" :img="'ards'" title="Ready to book?" text="Come and explore the beautiful regions of Northern Ireland with one of our talented tour guides."></seen-enough>
@endsection
@section('scripts')

@endsection