@php
$page = 'Contact';
$pagetitle = "Contact | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mt-5 mob-mt-0">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row pt-5 mt-5">
        <div class="col-lg-8  position-relative z-2">
            <h1 class="mb-4">Get in touch</h1>
            <p>Leave your details below and we will be in touch!</p>
            <p>Please view our <a href="/privacy-policy" class="text-blue">Privacy Statement & Cookie Policy</a> to see how we manage your data.</p>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-2">
	<div class="row">
		<div class="col-lg-8">
      <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
		</div>
		<div class="col-lg-4 pl-5 mob-px-3">
			<p><a href="mailto:info@tourguidesni.com" class="hover-blue"><i class="fa fa-envelope-o mr-1"></i> info@tourguidesni.com</a></p>
			<p><a href="tel:007704575932" class="hover-blue"><i class="fa fa-phone mr-1"></i> +44 (0) 7704 575 932</a></p>

			<p>
        <a href="https://www.facebook.com/tourguidesni" class="hover-blue"><i class="fa fa-facebook"></i></a>
        <a href="https://www.instagram.com/tourguidesni" class="hover-blue"><i class="fa fa-instagram ml-3"></i></a>
        <a href="https://twitter.com/Tour_GuidesNI" class="hover-blue" ><i class="fa fa-twitter ml-3"></i></a>
      </p>
		</div>
	</div>

</div>
<seen-enough :link="'/guides'" :btntext="'Book a guide'" :img="'belfast'" title="Ready to book?" text="Come and explore the beautiful regions of Northern Ireland with one of our talented tour guides."></seen-enough>
@endsection
@section('scripts')

@endsection