@php
$page = 'Guides';
$pagetitle = "Guides | Northern Ireland Tour Guides";
$metadescription = "Our guides embrace the giant spirit of Northern Ireland with a wonderful array of guest experiences from visiting world class attractions, to exploring the great outdoors, or savouring our vibrant food scene. Your next guided giant adventure is just 3 clicks away. Select your preferred region, tour type and language and meet the guides that will make your visit one to remember.";
$pagetype = 'light';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
$region = null;
if(isset($_GET['region'])) {
    $region = $_GET['region'];
}else{
    $region = '*';
}
echo $region;
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container position-relative pt-5 mt-5 mob-mt-0 text-center text-lg-left">
            <div class="row pt-5 mt-5">
                <div class="col-lg-8  position-relative z-2">
                    <h1 class="mb-4">Find your perfect tour guide</h1>
                    <p class="mb-4">Our guides embrace the giant spirit of Northern Ireland with a wonderful array of guest experiences from visiting world class attractions, to exploring the great outdoors, or savouring our vibrant food scene. Your next guided giant adventure is just 3 clicks away. Select your preferred region, tour type and language and meet the guides that will make your visit one to remember.</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="position-relative z-2">
    <div class="mb-5 mob-mb-0">
        <guides-index :region="'{{$region}}'" :type="'*'" :lang="'*'" :title="0" :search="''" :limit="16" :showregion="true" :lm="true"></guides-index>
    </div>
</div>
<seen-enough :link="'/contact'" :btntext="'Get in touch'" :img="'belfast'" title="Ready to book?" text="Come and explore the beautiful regions of Northern Ireland with one of our talented tour guides."></seen-enough>
@endsection
@section('scripts')

@endsection