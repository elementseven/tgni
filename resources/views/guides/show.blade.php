@php
$page = 'Profile';
$pagetitle = $profile->name . " | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'dark';
$pagename = 'profile';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative profile-header bg-grad text-dark position-relative z-3">
    <div class="row pt-5">
        <div class="container pt-5">
            <div class="row pt-5 mob-pt-0">
                <div class="col-12 z-2 d-lg-none text-center">
                    <p class="text-uppercase title letter-spacing mb-2 text-white">{{$profile->qualifications}}</p>
                    <h1 class="text-white">{{$profile->name}}</h1>
                    <p class="text-uppercase title text-small letter-spacing text-white">Languages @foreach($profile->languages as $lang) <img src="{{$lang->getFirstMediaUrl('languages')}}" alt="{{$lang->name}}" width="30" class="ml-2" /> @endforeach </p>
                </div>
                <div class="col-lg-3 mob-px-4">
                    <div class="mb-minus mob-mb-0 text-center text-lg-left" style="margin-bottom: -400px;">
                        <div class="card guide-header-card mb-5 mob-mb-4 d-inline-block" style="background-image: url('{{$profile->getFirstMediaUrl('profiles')}}');"></div>
                        <p class="text-small title letter-spacing text-uppercase">Contact Us</p>
                        @if($profile->website)
                        <p class="text-small mb-2" style="word-wrap: break-word;"><a href="{{$profile->website}}" class="text-dark" target="_blank"><i class="fa fa-globe mr-2"></i>{{preg_replace('/^www\./', '', preg_replace( "#^[^:/.]*[:/]+#i", "", preg_replace( "{/$}", "", urldecode( $profile->website ) ) ) )}}</a></p>
                        @endif
                        <p class="text-small mb-2" style="word-wrap: break-word;"><a href="mailto:{{$profile->email}}" class="text-dark"><i class="fa fa-envelope-o mr-2"></i>{{$profile->email}}</a></p>
                        <p class="text-small mb-4" style="word-wrap: break-word;"><a href="tel:{{$profile->phone}}" class="text-dark"><i class="fa fa-phone mr-2"></i>{{$profile->phone}}</a></p>
                        @if($profile->facebook != null || $profile->instagram != null || $profile->twitter != null || $profile->youtube != null || $profile->linkedin != null)
                        <p class="text-small title letter-spacing text-uppercase mb-2">Follow Us</p>
                        <p class="text-large letter-spacing mb-4">
                            @if($profile->facebook != null)<a href="{{$profile->facebook}}" class="text-dark" target="_blank"><i class="fa fa-facebook-square"></i></a>@endif
                            @if($profile->instagram != null)<a href="{{$profile->instagram}}" class="text-dark" target="_blank"><i class="fa fa-instagram"></i></a>@endif
                            @if($profile->twitter != null)<a href="{{$profile->twitter}}" class="text-dark" target="_blank"><i class="fa fa-twitter-square"></i></a>@endif
                            @if($profile->youtube != null)<a href="{{$profile->youtube}}" class="text-dark" target="_blank"><i class="fa fa-youtube-square"></i></a>@endif
                            @if($profile->linkedin != null)<a href="{{$profile->linkedin}}" class="text-dark" target="_blank"><i class="fa fa-linkedin-square"></i></a>@endif
                        </p>
                        @endif
                        <button class="btn btn-primary booknowbtn">Book this guide</button>
                    </div>
                </div>
                <div class="col-lg-9 z-2 px-5 mob-px-3 pt-5 d-none d-lg-block">
                    <p class="text-uppercase title letter-spacing mb-2 text-white">{{$profile->qualifications}}</p>
                    <h1 class="text-white">{{$profile->name}}</h1>
                    <p class="text-uppercase title text-small letter-spacing text-white">Languages @foreach($profile->languages as $lang) <img src="{{$lang->getFirstMediaUrl('languages')}}" alt="{{$lang->name}}" width="30" class="ml-2" /> @endforeach </p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container my-5 py-5 mob-mb-0 position-relative z-2">
            <div class="row mb-5 mob-mb-0">
                <div class="col-lg-9 offset-lg-3 pl-5 mob-px-3 text-center text-lg-left mob-px-4">
                    <h2>What do we offer?</h2>
                    <div class="profile-description">
                        {!!$profile->description!!}
                    </div>
                </div>
            </div>
            @if(count($profile->tours))
            <div class="row">
                <div class="col-lg-9 offset-lg-3 pl-5 mob-px-3 text-center text-lg-left mob-mt-5">
                    <p class="mimic-h2 mb-4">Specialist Tours</p>
                    <div class="row"> 
                        @foreach($profile->tours as $tour)
                        <div class="col-lg-6">
                            @if($tour->link)
                            <p><a href="{{$tour->link}}" class="text-dark" target="_blank"><i class="fa fa-link text-blue mr-2"></i> <u>{{$tour->name}}</u></a></p>
                            @else
                            <p><i class="fa fa-circle text-blue text-smallest mr-2"></i> {{$tour->name}}</p>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
            @if(count($profile->guideImages) > 0)
            <div class="row mt-5 mob-mt-0">
                <div class="col-12 mob-pl-0">
                    <guides-images-carousel :id="{{$profile->id}}"></guides-images-carousel>
                </div>
            </div>
            @endif
            @if($profile->id != 39)
            <div class="row mt-5 text-center text-lg-left">
                <div class="col-12">
                    <p class="mimic-h2">Regions</p>
                </div>
                @foreach($profile->regions as $region)  
                <div class="col-lg-3">
                    <div class="region-card mb-4 d-inline-block cards-{{$region->slug}}">
                        <a href="/regions/{{$region->slug}}">
                            <div class="region-info">
                          <p class="region-card-title">{{$region->name}}</p>
                        </div>
                      </a>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <div id="bookonline" class="row my-5 pb-5 mob-mb-0 mob-pb-0">
                <div class="col-lg-9 mob-px-4">
                    <p class="mimic-h2 text-center text-lg-left">Book {{$profile->name}}</p>
                    <contact-guide-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" :guide="{{$profile->id}}"></contact-guide-form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -150
      }, 500);
    });
  });
</script>
@endsection