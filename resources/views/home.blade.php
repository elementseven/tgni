@php
$page = 'Members Area';
$pagetitle = "Members Area | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.members', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mob-pb-0">
    <div class="row pt-5 mt-5 mob-mt-0">
        <div class="col-lg-8 position-relative z-2">
            <h1 class="mb-4">Welcome to your members area</h1>
            <p>Please browse the information available and let us know if we are missing anything you need.</p>
        </div>
    </div>
</header>
@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
@endsection
@section('scripts')

@endsection