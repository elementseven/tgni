@php
$page = 'Join';
$pagetitle = "Join | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'join';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container position-relative py-5 mt-5 mob-mt-0">
            <div class="row pt-5 mt-5">
                <div class="col-lg-12  position-relative z-2">
                    <h1 class="mb-4">Thinking of Joining Us?</h1>
                    <p class="text-large">Leave your details below and we will be in touch!</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-2">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mb-5">
                    <join-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></join-form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <h3>Who can join?</h3>
                    <p>We welcome all tour guides qualified at levels 2, 3 or 4. Membership is also open to those with a Blue Badge, and Failte Ireland tour guiding qualification.</p>
                    <p class="mb-4">If you are a tourism organisation or a tour operator you may be interested in our industry partner program. Our Honorary Membership brings together some of the leading experts in a range of subjects. For details of joining either program get in touch through our <a href="/contact" class="text-blue">contact page</a>.</p>

                    <h3 class="mt-5">How do I join?</h3>
                    <p>You can join by leaving your details in the form above. We will be in touch and send you a form to fill in with all the information we need to create your TGNI profile.</p>

                    <h3 class="mt-5">What are the benefits of joining?</h3>
                    <ul>
                        <li><p>General Tour Guide Liability Insurance </p></li>
                        <li><p>Quality Assurance, Continued Professional Development (CPD) & Training</p></li>
                        <li><p>Members only Fam Tours and site visits</p></li>
                        <li><p>Professional and Social Networking Opportunities</p></li>
                        <li><p>Advocacy for Tour Guiding Profession in Northern Ireland</p></li>
                        <li><p>Industry Partner Program</p></li>
                    </ul>
                    <p>Tour operators regularly look first to our membership when hiring professional tour guides in Northern Ireland. Join these ranks today!</p>

                    <h3 class="mt-5">What sort of training is available?</h3>

                    <p>Guides can avail of professional training at levels 2, 3 and 4. All levels are referenced to national qualifications frameworks and are therefore recognised throughout the UK and endorsed by Tourism NI and TGNI.</p>

                    <p>The <b>OCN NI Level 2 Award</b> is going strong in locations throughout Northern Ireland, with a number of guides gaining qualifications in Greater Belfast, the Mournes, the Glens and the Sperrins. A tour guide qualified at <b>Level 2</b> (yellow ID card and lanyard) will deliver <b>walking tours around designated sights or a specific route around a city, town, village or location</b>.</p>

                    <p>The <b>OCN NI Level 3 Certificate</b> is available through Belfast Met and other colleges throughout Northern Ireland. Local Councils and Landscape Partnerships are also planning to offer these. A guide at <b>Level 3</b> (green ID card and lanyard) will deliver <b>city, themed coach-, or walking tours</b>.</p>

                    <p>TGNI is proud to have played a vital role in the development of the <b>Level 4 Tour Guiding Certificate in Higher Education</b>. Delivered by Belfast Metropolitan College and validated by Ulster University, Level 4 offers the highest level of qualification for professional tour guiding currently available in Northern Ireland. Tour guides at <b>Level 4</b> (blue ID card and lanyard) are qualified to offer <b>tours around Northern Ireland and all Ireland by coach or walking, including tours lasting a number of days</b>.</p>
                    <p class="mb-0 text-small"><i>For more information go to:</i></p>
                    <p><a href="https://www.belfastmet.ac.uk/courses/?keyword=tour%20guiding" class="text-blue">https://www.belfastmet.ac.uk/courses/?keyword=tour%20guiding</a></p>

                    <p><b>Language guides</b> not yet qualified at levels 2, 3 or 4 (grey ID card and lanyard) are guiding in their respective languages.</p>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-5 position-relative z-2">
        <div class="row py-5 mob-py-0 my-5">
            <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
                <div class="d-table w-100 h-100">
                    <div class="d-table-cell align-middle w-100 h-100 text-lg-left">
                        <p class="mimic-h2">Have a question?</p>
                        <p>Get in touch with Tour Guides NI using the form on our contact page.</p>
                        <a href="/contact">
                            <button class="btn btn-primary">Get in touch</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pr-0 mob-px-4">
                <div class="right-img">
                    <picture>
                        <source srcset="/img/regions/ards.webp" type="image/webp"/> 
                        <source srcset="/img/regions/ards.jpg" type="image/jpeg"/> 
                        <img src="/img/regions/ards.jpg" type="image/jpeg" alt="Ards- SCRABO" class=" w-100" />
                    </picture>
                </div>
            </div>
            <div class="col-12 px-4 d-lg-none text-center mt-4">
                <div class="d-table w-100 h-100">
                    <div class="d-table-cell align-middle w-100 h-100 text-lg-left">
                        <p class="mimic-h2 mb-2">Have a question?</p>
                        <p class="mb-4">Get in touch with Tour Guides NI using the form on our contact page.</p>
                        <a href="/contact">
                            <button class="btn btn-primary">Get in touch</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection