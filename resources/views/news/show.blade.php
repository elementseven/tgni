@php
$page = 'News';
$pagetitle = $post->title . ' | Northern Ireland Tour Guides';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://tourguidesni.com' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.members', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mob-mb-0">
  <div class="row pt-5">
    <div class="container mob-pt-5">
      <div class="row  pt-5 pb-4 mob-pt-0">
        <div class="col-lg-9 pt-5 ipadp-pt-0 mob-pt-0 mob-mt-0 text-center text-lg-left mob-mb-3">

          <div class="pre-title-lines mb-2 mob-my-45 mob-mx-auto mob-mb-3 text-uppercase"><span class="d-none d-lg-inline"><a href="{{route('news.index')}}" class="text-blue"><i class="fa fa-newspaper-o mr-1"></i> <b>Browse all news</b></a></span></div>
          <p class="d-lg-none text-large text-uppercase"><a href="{{route('news.index')}}" class="text-blue"><i class="fa fa-newspaper-o mr-1"></i> <b>Browse all news</b></a></p>
          <h1 class="mb-3 mob-mb-0 blog-title">{{$post->title}}</h1>
          <p class="mb-4 text-large">{{$post->excerpt}}</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
  <div class="row pb-5 mob-py-0 text-center text-lg-left">
    <div class="col-lg-9  mob-mt-0 blog-body">
      {!!$post->content!!}
    </div>
    @if(count($post->newsDocuments))
    <div class="col-lg-3 mob-mt-5 text-left">
      <div class="border-left pl-4 h-100">
        <h4 class="mb-0 text-capitalize">Downloads</h4>
        <p class="text-smallest">* Click the document name to download</p>
        @foreach($post->newsDocuments as $key => $doc)
        <p class="mb-2"><a href="{{$doc->getFirstMediaUrl('newsdocuments')}}" target="_blank" class="text-blue"><i class="fa fa-file-text-o mr-1"></i> <b>{{$doc->name}}</b></a></p>
        @if($key < count($post->newsDocuments) - 1)<hr class="my-1"/>@endif
        @endforeach
      </div>
    </div>
    @endif
  </div>
</div>
<div class="container pb-5 mob-mt-5 mob-px-4 position-relative z-2">
  <div class="row mb-5">
    <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2 mob-smaller"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">More TGNI news</span></p>
      <a href="{{route('news.index')}}"><button type="button" class="btn btn-primary mob-mt-2">See all news</button></a>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection