@php
$page = 'Privacy Policy';
$pagetitle = "Privacy Policy | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'tandcs';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mt-5 mob-mt-0">
	<div class="row pt-5 mt-5">
		<div class="col-lg-8  position-relative z-2">
			<h1 class="mb-4">Privacy Policy</h1>
			<p class="mb-4">Last updated: 21/08/2021</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 position-relative z-2">
	<div class="row pb-5 mb-5">
		<div class="col-12">
			<p><strong><br>
			About Us</strong></p>
			<p>Tour Guides NI is a&nbsp;Tour Guiding Body which acts as a vehicle of progression for Northern Ireland Tour Guides.&nbsp; We can be contacted via&nbsp;https://tourguidesni.com/contact-tour-guides-ni/</p>
			<p><strong>We respect your privacy</strong></p>
			<p>This policy sets out the basis on which any personal data the business collects from you, or that you provide to the business, will be processed by us.</p>
			<p>It explains what information we collect, how we use it, who we share it with and how we protect it. It also details the rights available to you in relation to how we hold and use your personal data, how to exercise those rights, and what to do if you require more information or wish to make a complaint.</p>
			<p>This privacy notice applies to all personal data we hold.&nbsp; For example, we may hold information about current &amp; previous customers, prospective customers and competition entrants etc.</p>
			<p><strong>Why we collect your Personal Data</strong></p>
			<p>We collect your personal data so that we can manage our relationships with you. Activities that we require personal data for include:</p>
			<ul>
			<li>Performance and maintenance of a contract</li>
			<li>Provision of other services</li>
			<li>Improving our existing products and services</li>
			<li>Developing new products and services</li>
			<li>Responding to requests and providing information</li>
			<li>A range of other activities which we are obliged to undertake, or which we have gained consent to complete.</li>
			</ul>
			<p>We ensure that any information we collect relates to the purposes for which it is obtained.</p>
			<p><strong>Security of your Personal Data</strong></p>
			<p>We keep our computer systems secure by following legal requirements and international security guidance. We make sure that our staff are fully trained on how to protect personal data.&nbsp; We ensure that our processes clearly identify the requirements for managing personal data and that they are up to date. We regularly audit our systems and processes to ensure that we remain compliant with these policies and legal obligations. We do not store any off-line personal data.</p>
			<p><strong>Types of Personal Data we may hold </strong><br>
			You can access and browse our site without disclosing any personal data. However; the business may collect and process the following data about you:</p>
			<ul>
			<li>Information about you and your business: this includes, but is not limited to, enquiry forms, email links, posting material or requesting further services, your current and past products ordered</li>
			<li>Information about account balances and outstanding balances</li>
			<li>Information about how you use products &amp; services supplied by us</li>
			</ul>
			<p>We collect most of this information directly from the individuals that we interact with, when they contact by telephone or electronically. We may also obtain information about individuals from external parties.</p>
			<p><strong>Use of Cookies</strong></p>
			<p>The business may obtain information about your general internet usage by using a cookie file which is stored on the hard drive of your computer. Cookies contain information that is transferred to your computer’s hard drive. They help us to improve our site and to deliver a better and more personalised service.</p>
			<p>They enable the business: to estimate our audience size and usage pattern; to store information about your preferences, and so allow us to customise our site according to your individual interests; to speed up your searches; to recognise you when you return to our site.</p>
			<p>Further information about the type of cookies that we use and their purpose is available in the Cookies Policy in Part 2 of this document.</p>
			<p>We do not use cookies to gather any personal data for storage on our systems. You may refuse to accept cookies by activating the setting on your browser which allows you to refuse the setting of cookies. However, if you select this setting you may be unable to access certain parts of our site. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you log on to our site. Please note that our advertisers may also use cookies, over which the business have no control.</p>
			<p><strong>&nbsp;How long we keep data</strong></p>
			<p>Information collected by us will be held for as long as it is required for the purpose it was collected and to protect our business &amp; rights.&nbsp; By law, we are required to keep certain types of information for a specific period of time. The length of time we keep your personal data depends on the type of information and the purpose for which it was obtained.</p>
			<p><strong>&nbsp;How we may use Personal Data</strong></p>
			<p>The business may use information held about you in the following ways:</p>
			<ul>
			<li>Providing requested information about products</li>
			<li>Setting up new accounts and billing details</li>
			<li>Monitoring product usage</li>
			<li>Generating and sending invoices</li>
			<li>Monitoring payment activity and managing debt</li>
			<li>Discussing and responding to requests for information or general queries</li>
			<li>To enable us to comply with any legal or regulatory requirements</li>
			<li>To protect or enforce our rights or the rights of any third party</li>
			<li>In the detection and prevention of fraud and other crime</li>
			<li>For the purpose of safeguarding someone’s vital interests, national security, responding to statutory obligations or requests from the courts and enforcement authorities.</li>
			</ul>
			<p><strong><br>
			Activities that require your consent</strong></p>
			<p>In order for us to carry out certain activities using your personal data, we may need to ask for your consent. For example, in order to allow us to make contact with you via email or SMS to make you aware of new products that may be of interest to you, we need you to have provided us with prior consent to do so.</p>
			<p>When consent is being requested, we will provide options such as the choice of whether we may contact you by phone, post, email, text or through other digital media.</p>
			<p>Where we require consent, we will explain why and provide sufficient information to allow you to make an informed decision</p>
			<p>When we receive consent to perform such activities, that consent may be withdrawn at any time by contacting us.</p>
			<p><strong>Who we share information with</strong></p>
			<p>We may share your personal data with, or disclose your personal data to, the following categories of third party:</p>
			<ul>
			<li><strong>Agents or suppliers:</strong>these are persons or companies we have contracts with to provide products or services that we use in conducting our business, including managing our relationship with our customers. In many cases, they will be within the European Economic Area (EEA) but in some cases they may be outside of the EEA. We will only share or disclose to these parties the information that they need in order to provide the products or services and will require those parties to ensure that the information is always adequately protected.</li>
			<li><strong>Professional advisers:&nbsp;</strong>we may share or disclose personal data to professional advisers we may engage for any reasonable purpose in connection with our business, including assistance in protecting our rights</li>
			<li><strong>Other external bodies:&nbsp;</strong>in certain circumstances, we may be required by law to disclose personal data to external bodies, such as local authorities, government departments or PSNI.&nbsp; In these cases, we will only disclose the minimum amount of information required to satisfy our legal obligation.&nbsp; However, once the information is disclosed, we will not be able to control how it is used by those bodies.</li>
			</ul>
			<p><strong><br>
			How to contact us</strong></p>
			<p>If you have any queries or comments about this privacy notice, we can be contacted via&nbsp;https://tourguidesni.com/contact-tour-guides-ni/</p>
			<p><strong>How we address your rights</strong></p>
			<p>We capture, store and process your personal information to provide you with a range of services. You have a range of rights available to you to give you confidence that your information is appropriately managed.</p>
			<p>The rights that you have available to you include:</p>
			<p><strong><em>Gaining access to your personal data</em></strong><strong>:&nbsp;</strong>you are entitled to receive, on request and free of charge, a copy of all your personal data that we hold. There are some limitations to this right – e.g. if the data also relates to another person and we do not have that person’s consent, or if the data is subject to legal privilege. Where there is data that we cannot disclose, we will explain this to you.</p>
			<p><strong><em>Ensuring that your data is accurate:&nbsp;</em></strong>our aim is to ensure that the data we hold about you is correct and up to date. From time to time we may contact you to verify the information that we hold. You may also contact us to correct any errors that you notice.</p>
			<p><strong><em>Granting or Removing consent:</em></strong>&nbsp;where we require your consent for any processing, for example, to provide you with direct marketing communications, we will clearly explain what the consent is for, and any consequences of giving or refusing consent, and will provide that consent can only be given by way of a positive action by you. We will also ensure that you are able to withdraw any such consent at any time.</p>
			<p><strong><em>Restricting processing of your data:</em></strong>&nbsp;you have the right to request us to restrict the processing of your personal data in certain circumstances, for example, if there is a dispute over our rights to carry out specific processing activities, or where you do not want us to delete data.&nbsp; We will respond promptly to your request and will provide an explanation if we cannot fully comply.</p>
			<p><strong><em>Deletion of your data:</em></strong>&nbsp;in certain circumstances, you may have the right to have some or all of your personal data deleted from our records. This is sometimes referred to as the “right to be forgotten”. This may occur if, for example, we retain data which is no longer required by us, or if you withdraw a consent.&nbsp; If you continue to have a relationship with us, we must retain the data we need to manage this relationship. We will respond promptly to your request and provide reasons if we object to the deletion of any of your personal data.</p>
			<p><strong><em>Moving your data:&nbsp;</em></strong>where it is possible for us to provide it, you have the right to receive a digital copy of the personal data that you have provided to us.</p>
			<p><strong>International Transfers of Data:&nbsp;</strong>in certain circumstances, we may transfer your personal information internationally, including outside of the European Economic Area (EEA). Should we do this, we ensure that all transfers are made in accordance with data protection law and that your data will be given an equivalent level of protection that it has when it is being managed in the UK.<br>
			<strong>How to make a complaint</strong></p>
			<p>If for any reason you have a complaint about our use of your personal information, or you are unhappy in any way with the information we provide to you, we would like you to contact us directly so that we can address your complaint. You can contact us via:&nbsp;https://tourguidesni.com/contact-tour-guides-ni/</p>
			<p><strong>Changes to our privacy notice</strong></p>
			<p>We will occasionally make updates to this privacy notice. Any changes we make to this privacy notice in the future will be posted on this page and, where appropriate, notified to you by e-mail or any of the contact details we hold for you for this purpose. We encourage you to periodically review this notice to be informed about how we use your information.</p>
			<p><strong>&nbsp;</strong></p>
			<p><strong><u>COOKIES POLICY</u></strong></p>
			<p><strong>What is a cookie</strong></p>
			<p>The business may obtain information about your general internet usage by using a cookie file which is stored on the hard drive of your computer. Cookies contain information that is transferred to your computer’s hard drive. They help us to improve our site and to deliver a better and more personalised service.</p>
			<p>Cookies enable the business: to estimate our audience size and usage pattern; to store information about your preferences, and so allow us to customise our site according to your individual interests; to speed up your searches; to recognise you when you return to our site; enable you to shop online, storing items as you add them to your virtual shopping basket.</p>
			<p><strong>Types of cookie</strong></p>
			<p>Cookies can be either temporary (session cookie) or permanent (persistent cookie).</p>
			<p>Session cookies are stored in your device’s temporary memory – not on your hard drive – while you’re browsing a website. Usually these cookies are deleted when you close the browser. If you were to reopen the browser and revisit the website, the site would not ‘remember’ that you had visited previously. Session cookies remain active only until you leave a site.</p>
			<p>Persistent cookies remain stored on your hard drive, persisting from session to session until you delete them or they reach a set expiration date. Persistent cookies can store information such as log-in details, bookmarks, credit card details and preferred settings and themes – resulting in a faster and smoother web journey.</p>
			<p><strong>How to control cookies</strong></p>
			<p>You can control and/or delete cookies as you wish – for details on how to do this, see&nbsp;<u>aboutcookies.org</u>&nbsp; You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.<br>
			<strong>How we use cookies</strong></p>
			<p>The cookies we use on our website can be grouped into four different categories.</p>
			<p><strong>Strictly necessary cookies:</strong>&nbsp;are essential in order to enable you to navigate around our website and use its features. Without these cookies, we would be unable to provide you with the products you have asked for.</p>
			<p><strong>Functionality cookies:</strong>&nbsp;allows our website to remember choices you make and help to provide an enhanced, more personal experience on our website.</p>
			<p><strong>Performance cookies:</strong>&nbsp;helps us improve our website and our online services. These cookies gather information about how our site is used, including which pages are visited most often. This helps us to provide a better user experience. These cookies are anonymous – which means that they won’t collect information to identify you.</p>
			<p><strong>Targeting &amp; Advertising cookies:</strong>&nbsp;are used to help us better understand our advertising campaigns and how we can make these more relevant to you. These cookies are also anonymous, they won’t collect information to identify you.</p>
		</div>
	</div>
</div>
@endsection