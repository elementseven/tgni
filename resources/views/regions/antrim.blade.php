@php
$page = 'Mid & East Antrim';
$pagetitle = "Mid & East Antrim | Regions | Northern Ireland Tour Guides";
$metadescription = "The starting point to the world- famous Antrim Coastal Road, let our guides join you on new and enchanting adventures.";
$pagetype = 'dark';
$pagename = 'mid-east-antrim';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-antrim bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Glens of Antrim</p>
			        <p class="region-title">Mid & East Antrim</p>
			        <p>The starting point to the world- famous Antrim Coastal Road, let our guides join you on new and enchanting adventures.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Mid & East Antrim</p>
		            <p class="mb-4 mob-mb-3">Join local guide Clare O’Neill and Mary Watson from Carnlough Walking Tours explore and discover the Mid-East Antrim region. Seen as the gateway to the Glens of Antrim, this region is a washed with hidden gems such as Glenariff waterfall.  If you wanted to know more about castles, this is the region for you, you can even stay over in the luxurious BallyGally Castle, just beware of its ghostly past. Get in touch with Clare or another local guide to plan your next trip to the Mid-East Antrim region</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/xS5DBJbQ8Fo?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/antrim/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/antrim/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/antrim/1.jpg" type="image/jpeg" alt="Gateway to the Glens - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Gateway to the Glens</p>
                    <p class="mb-4">This region and its historic towns such as Carnlough are a perfect place to plan a getaway in the superb scenic Glens of Antrim.  Contact a local guide to discover more.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Getaway Paradise</p>
                    <p class="mb-4">Ballygally Castle hotel and Galgorm Spa and Gold Resort are two iconic venues in our region that offers the perfect base to any memorable holiday on our shores.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/antrim/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/antrim/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/antrim/2.jpg" type="image/jpeg" alt="Getaway Paradise - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Getaway Paradise</p>
                    <p class="mb-4">Ballygally Castle hotel and Galgorm Spa and Gold Resort are two iconic venues in our region that offers the perfect base to any memorable holiday on our shores.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/antrim/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/antrim/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/antrim/3.jpg" type="image/jpeg" alt="Natural storytellers - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Natural storytellers</p>
                    <p class="mb-4">The Glens of Antrim are infamous for its rich traditions and heritage including Storytelling.  From the lady in the bakery to the local publican, natural storytellers are abound.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">A Victorian work of wonder</p>
                    <p class="mb-4">The Gobbins is one of the islands, hidden gems.  Renovations has modernised this walk of natural beauty, witnessing the clash between land and sea at first sight.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/antrim/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/antrim/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/antrim/4.jpg" type="image/jpeg" alt="A Victorian work of wonder - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">A Victorian work of wonder</p>
                    <p class="mb-4">The Gobbins is one of the islands, hidden gems.  Renovations has modernised this walk of natural beauty, witnessing the clash between land and sea at first sight.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
		<div class="container">
		    <div class="row">
    	        <div class="col-12 mob-px-4 text-center text-lg-left">
    	            <h2 class="mob-mb-2 mb-4">Our Guides for Mid & East Antrim</h2>
    	        </div>
                <guides-index :region="'mid-east-antrim'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="8" :search="''"></guides-index>
                <div class="col-12 text-center mt-4">
                    <a href="/guides?region=mid-east-antrim">
                        <button class="btn btn-primary" type="button">View more Guides</button>
                    </a>
                </div>
	       </div>
        </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'mid-east-antrim'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'mid-east-antrim'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection