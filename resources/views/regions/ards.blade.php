@php
$page = 'Ards & North Down';
$pagetitle = "Ards & North Down | Regions | Northern Ireland Tour Guides";
$metadescription = "The picturesque hills of Ards mixed with award winning beaches of North Down, makes this region an ideal setting for wandering and wondering in the wild.";
$pagetype = 'dark';
$pagename = 'ards-north-down';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-ards bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Scrabo Tower</p>
			        <p class="region-title">Ards & North Down</p>
			        <p>The picturesque hills of Ards mixed with award winning beaches of North Down, makes this region an ideal setting for wandering and wondering in the wild.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Ards & North Down</p>
		            <p class="mb-4 mob-mb-3">Join local guides Richard Orr from Bestfast Tours and TGNI Chairperson Ken Martin explore the Ards and North Down Region. This region is in close proximity to capital city Belfast and is a great area to discover the spiritual and religious history of the lands.  From monastic sites to bespoke museums, this region is a fantastic place for the culturally curious. Get in touch with Richard, Ken or another local guide to plan your next trip to the Ards and North Down region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/yetDP96kJT4?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/ards/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/ards/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/ards/1.jpg" type="image/jpeg" alt="Mystical Down - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Mystical Down</p>
                    <p class="mb-4">This region is amassed with spiritual and religious places of interest.  Sites such as Nendrum Monastery are a perfect place to hear stories from our guides and great for Christian heritage trails.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Museum of thoughts</p>
                    <p class="mb-4">We love our history, so we do.  Let one of our guides bring you around the area’s many museums to get an understanding of our complexities and diversities while visiting the area.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/ards/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/ards/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/ards/2.jpg" type="image/jpeg" alt="Museum of thoughts - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Museum of thoughts</p>
                    <p class="mb-4">We love our history, so we do.  Let one of our guides bring you around the area’s many museums to get an understanding of our complexities and diversities while visiting the area.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/ards/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/ards/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/ards/3.jpg" type="image/jpeg" alt="Strangford Lough - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Strangford Lough</p>
                    <p class="mb-4">One of the most richly bio-diverse regions in Europe, Strangford Lough is also an Area of Special Scientific Interest as well as a Area of Outstanding Natural Beauty.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Place of the Potato</p>
                    <p class="mb-4">Home to some of the country’s best farming produce, make sure to get well fed on your travels.  Our local spud, the Comber, is even protected, a product of Protected Geographical Indication (PGI) status.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/ards/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/ards/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/ards/events/4.jpg" type="image/jpeg" alt="Place of the Potato - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Place of the Potato</p>
                    <p class="mb-4">Home to some of the country’s best farming produce, make sure to get well fed on your travels.  Our local spud, the Comber, is even protected, a product of Protected Geographical Indication (PGI) status.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Ards & North Down</h2>
	        </div>
            <guides-index :region="'ards-north-down'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="8" :search="''"></guides-index>
            <div class="col-12 text-center mt-4">
                <a href="/guides?region=ards-north-down">
                    <button class="btn btn-primary" type="button">View more Guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'ards-north-down'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'ards-north-down'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection