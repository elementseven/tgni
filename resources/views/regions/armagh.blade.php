@php
$page = 'Armagh, Banbridge & Craigavon';
$pagetitle = "Armagh, Banbridge & Craigavon | Regions | Northern Ireland Tour Guides";
$metadescription = "Hear and learn about the rich Christian heritage of this region, just one of the many facets on offer to make your visit a memorable one.";
$pagetype = 'dark';
$pagename = 'armagh-banbridge-craigavon';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-armagh bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Nature & Culture</p>
			        <p class="region-title">Armagh, Banbridge <br/>& Craigavon</p>
			        <p>Hear and learn about the rich Christian heritage of this region, just one of the many facets on offer to make your visit a memorable one.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Armagh, Banbridge & Craigavon</p>
		            <p class="mb-4 mob-mb-3">Join local guide and TGNI chairperson Ken Martin explore and discover the Armagh, Banbridge and Craigavon region. A perfect place for visitors or groups interested in the Christian heritage of our lands.  This is another region that has culture and arts at its heart, be sure to check what’s on in the area when visiting. Get in touch with Ken or another local guide to plan your next trip to the Armagh, Banbridge and Craigavon region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cQ7L6m944qs?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/armagh/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/armagh/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/armagh/1.jpg" type="image/jpeg" alt="Christian Heritage - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Christian Heritage</p>
                    <p class="mb-4">Armagh City is the ecclesiastical capital of Ireland and a perfect place to explore the Christian heritage of our lands.  Get in touch with one of our guides to find out more.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Worlds best Apple</p>
                    <p class="mb-4">The Armagh Bramley Apple, product of UNESCO ‘Protected Geographical Indication’ is seen by many around the world as best apple for cider making, come visit and sample for yourself.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/armagh/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/armagh/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/armagh/2.jpg" type="image/jpeg" alt="Worlds best Apple - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Worlds best Apple</p>
                    <p class="mb-4">The Armagh Bramley Apple, product of UNESCO ‘Protected Geographical Indication’ is seen by many around the world as best apple for cider making, come visit and sample for yourself.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/armagh/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/armagh/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/armagh/3.jpg" type="image/jpeg" alt="Reach for the stars - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Reach for the stars</p>
                    <p class="mb-4">With over 50 years experience, the Armagh Planetarium is the longest running in the British Isles.  A fantastic addition to the region that is great for families.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Nature tours</p>
                    <p class="mb-4">Our region is home to spectacular places of beauty.  Be sure to explore our landscapes, for the nature lovers, Oxford Island and Lough Neagh Discovery Centre is a must.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/armagh/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/armagh/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/armagh/events/4.jpg" type="image/jpeg" alt="Nature tours - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Nature tours</p>
                    <p class="mb-4">Our region is home to spectacular places of beauty.  Be sure to explore our landscapes, for the nature lovers, Oxford Island and Lough Neagh Discovery Centre is a must.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Armagh, Banbridge & Craigavon</h2>
	        </div>
            <guides-index :region="'armagh-banbridge-craigavon'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="8" :search="''"></guides-index>
            <div class="col-12 text-center mt-4">
                <a href="/guides?region=armagh-banbridge-craigavon">
                    <button class="btn btn-primary" type="button">View more Guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'armagh-banbridge-craigavon'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'armagh-banbridge-craigavon'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection