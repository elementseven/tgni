@php
$page = 'Belfast City';
$pagetitle = "Belfast City | Regions | Northern Ireland Tour Guides";
$metadescription = "Officially the best ‘wee’ city in the world.  No trip to our shores is worthwhile, without interacting and engaging with the people of Belfast and their stories.";
$pagetype = 'dark';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-belfast bg z-2">
    <span class="trans"></span>
    <div class="row">
        <div class="container container-wide mob-px-4">
            <div class="row">
              <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
                <div class="d-table w-100 h-100">
                  <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
                    <p class="region-top text-uppercase">TITANIC BELFAST</p>
                    <p class="region-title">Belfast City</p>
                    <p>Officially the best ‘wee’ city in the world.  No trip to our shores is worthwhile, without interacting and engaging with the people of Belfast and their stories.</p>
                    <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
            <div class="row py-5 mob-py-0">
                <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
                    <p class="mimic-h2 mb-2">Belfast</p>
                    <p class="mb-4 mob-mb-3">Join local guides Paul Moyna from Belfast Walking Tours and Rodney Ferguson from Cavehill Walking Tours as they explore their city of Belfast. Belfast is a city of many contrasts and stories.  As the city rises again with Tourism at its heart, there is options for all types of visitors, from walking tours to visiting the iconic Titanic centre, this region has it all.  Be sure to sample the city’s legendary food scene and lively pubs. Get in touch with Paul, Conor or another local guide to plan your next trip to Belfast.</p>
                    <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
                </div>
                <div class="col-lg-7 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qNp-b1r4h9c?rel=0" allowfullscreen></iframe>
                    </div>
                    <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/belfast/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/belfast/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/belfast/events/1.jpg" type="image/jpeg" alt="A Titanic tale - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">A Titanic tale</p>
                    <p class="mb-4">Just like our city’s rich heritage before it, Titanic Belfast has been a shining example of the new Belfast.  World Travel Awards leading tourist attraction winner in 2016.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Worlds biggest<br/>art gallery</p>
                    <p class="mb-4">Witness how our city has grown, and showed the world that our city adapts and thrives when challenged.  With outbursts of modern bright art in our concrete jungles, art is our virtue.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/belfast/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/belfast/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/belfast/events/2.jpg" type="image/jpeg" alt="Worlds biggest art gallery - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Worlds biggest art gallery</p>
                    <p class="mb-4">Witness how our city has grown, and showed the world that our city adapts and thrives when challenged.  With outbursts of modern bright art in our concrete jungles, art is our virtue.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/belfast/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/belfast/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/belfast/events/3.jpg" type="image/jpeg" alt="Walk our streets - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Walk our streets</p>
                    <p class="mb-4">The famous Belfast charm and wit is alive and well in our local tour guiding world.  Contact a guide to hear the true characters and stories of our majestic city of Belfast.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">A city of music<br/>and life</p>
                    <p class="mb-4">Belfast is blessed with numerous musical movements still alive and kicking such as dance music and punk rock.  Get in touch with a local guide to experience music and events on your visit.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/belfast/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/belfast/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/belfast/events/4.jpg" type="image/jpeg" alt="A city of music and life - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">A city of music and life</p>
                    <p class="mb-4">Belfast is blessed with numerous musical movements still alive and kicking such as dance music and punk rock.  Get in touch with a local guide to experience music and events on your visit.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
            <div class="container">
            <div class="row">
            <div class="col-12 mob-px-4 text-center text-lg-left">
                <h2 class="mob-mb-2 mb-4">Our Guides for Belfast City</h2>
            </div>
            <guides-index :region="'belfast-city'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="8" :search="''"></guides-index>
            <div class="col-12 text-center mt-4">
                <a href="/guides?region=belfast-city">
                    <button class="btn btn-primary" type="button">View more Guides</button>
                </a>
            </div>
        </div>
      </div>
    </div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'belfast'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'belfast'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
    window.onload = (event) => {
    var w = parseInt($(window).innerWidth());
    var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
        if(w >= 767){
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top - 150
          }, 500);
        }else{
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top + 0
          }, 500);
        }
    });
  };
</script>
@endsection