@php
$page = 'Causeway Coast & Glens';
$pagetitle = "Causeway Coast & Glens | Regions | Northern Ireland Tour Guides";
$metadescription = "Come, explore and visit the gateway to the 8th Wonder of the World, The Giants Causeway.  Home to magical places such as the world's oldest whiskey distillery.";
$pagetype = 'dark';
$pagename = 'causeway-coast-glens';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-causeway bg z-2">
    <span class="trans"></span>
    <div class="row">
        <div class="container container-wide mob-px-4">
            <div class="row">
              <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
                <div class="d-table w-100 h-100">
                  <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
                    <p class="region-top text-uppercase">GIANTS CAUSEWAY</p>
                    <p class="region-title">Causeway Coast<br/>& Glens</p>
                    <p>Come, explore and visit the gateway to the 8th Wonder of the World, The Giants Causeway.  Home to magical places such as the world's oldest whiskey distillery.</p>
                    <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
                  </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
            <div class="row py-5 mob-py-0">
                <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
                    <p class="mimic-h2 mb-2">Causeway Coast & Glens</p>
                    <p class="mb-4 mob-mb-3">Join local guide Clare O’Neill explore and discover the Causeway Coast and Glens region. This region is spectacular for breath-taking views and places of interest.  Join Clare as she visits one of the country’s best bakeries Ursa Minor while showcasing local businesses as North Coast Smokehouse and even drops in on legendary Storyteller Liz Weir at her barn. Get in touch with Clare or another local guide to plan your next trip to the Causeway Coast and Glens.</p>
                    <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
                </div>
                <div class="col-lg-7 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/WBNTtkf8zqc?rel=0" allowfullscreen></iframe>
                    </div>
                    <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/causeway/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/causeway/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/causeway/1.jpg" type="image/jpeg" alt="Spectacular scenery - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Spectacular scenery</p>
                    <p class="mb-4">This region is potted with majestic landscapes and scenery.  Some of our favourites include Ballintoy Harbour and Carrick-a-rede bridge.  Get in touch with a guide to plan your ideal adventure</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Support the local economy</p>
                    <p class="mb-4">This region is renowned for its support of the local economy, this includes a thriving économusée community including award winning bakers to preserving the local tradition of a smokehouse.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/causeway/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/causeway/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/causeway/2.jpg" type="image/jpeg" alt="Support the local economy - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Support the local economy</p>
                    <p class="mb-4">This region is renowned for its support of the local economy, this includes a thriving économusée community including award winning bakers to preserving the local tradition of a smokehouse.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/causeway/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/causeway/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/causeway/events/3.jpg" type="image/jpeg" alt="Worlds oldest whiskey distillery - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Worlds oldest whiskey distillery</p>
                    <p class="mb-4">With over 400 years of distilling history, Bushmills is the oldest whiskey distillery in the world.  Visit the iconic site and hear the stories and science behind the drink translated in Irish as ‘Water of Life’.</p>
                   <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Discover local wildlife</p>
                    <p class="mb-4">As well as Blue Flag beaches scattered along our precious coastline, our region is a fantastic place to discover our local wildlife including a recent addition of dolphins to our native seas.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/causeway/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/causeway/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/causeway/4.jpg" type="image/jpeg" alt="Discover local wildlife - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Discover local wildlife</p>
                    <p class="mb-4">As well as Blue Flag beaches scattered along our precious coastline, our region is a fantastic place to discover our local wildlife including a recent addition of dolphins to our native seas.</p>
                    <button class="btn btn-primary">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
            <div class="container">
            <div class="row">
            <div class="col-12 mob-px-4 text-center text-lg-left">
                <h2 class="mob-mb-2 mb-4">Our Guides for Causeway Coast & Glens</h2>
            </div>
            <guides-index :region="'causeway-coast-glens'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=causeway-coast-glens">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
        </div>
      </div>
    </div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'causeway-coast-glens'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'causeway-coast-glens'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
    window.onload = (event) => {
    var w = parseInt($(window).innerWidth());
    var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
        if(w >= 767){
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top - 150
          }, 500);
        }else{
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top + 0
          }, 500);
        }
    });
  };
</script>
@endsection