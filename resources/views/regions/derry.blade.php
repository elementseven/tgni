@php
$page = 'Derry City & Strabane';
$pagetitle = "Derry City & Strabane | Regions | Northern Ireland Tour Guides";
$metadescription = "A LegenDerry welcome like no other.  Come visit the Maiden city and its surrounding area. Experience for yourself what makes its history, people and culture so special.";
$pagetype = 'dark';
$pagename = 'derry-strabane';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-derry bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Derry's Walls</p>
			        <p class="region-title">Derry City<br/>& Strabane</p>
			        <p>A LegenDerry welcome like no other.  Come visit the Maiden city and its surrounding area. Experience for yourself what makes its history, people and culture so special.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Derry City & Strabane</p>
		            <p class="mb-4 mob-mb-3">Join local guide David Douglas from Derry Danders explore and discover the Derry and Strabane region. David is a fantastic ambassador for the region and gives us a small glimpse to the range of history and places of interest in the region. David even meets local guide and ecologist Martin Bradley as they investigate the wonders of the natural world. Get in touch with David or another local guide to plan your next trip to the Derry and Strabane region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/lW8bL1vztng?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/derry/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/derry/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/derry/1.jpg" type="image/jpeg" alt="Walk our walled city - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Walk our walled city</p>
                    <p class="mb-4">Derry City history is unique and compelling, often labelled the Maiden City.  Contact one of our fantastic guides to organise a walk around Irelands only walled city.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Halloween in Derry</p>
                    <p class="mb-4">Our region has a established reputation for being one of the best places on the planet to celebrate Halloween.  Get in touch with one of our guides to plan a Halloween to remember.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/derry/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/derry/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/derry/2.jpg" type="image/jpeg" alt="Halloween in Derry - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Halloween in Derry</p>
                    <p class="mb-4">Our region has a established reputation for being one of the best places on the planet to celebrate Halloween.  Get in touch with one of our guides to plan a Halloween to remember.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/derry/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/derry/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/derry/3.jpg" type="image/jpeg" alt="Arts and Culture - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Arts and Culture</p>
                    <p class="mb-4">Derry city has in recent years became a top spot for internationally recognised street art.  On top of our buzzing music and arts scene, any culturally curious visitors will love our region.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">History and Politics</p>
                    <p class="mb-4">From our bespoke museums to the stories of our people, our region is the perfect place to engage and understand the historical and political complexities of our land.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/derry/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/derry/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/derry/events/4.jpg" type="image/jpeg" alt="History and Politics - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">History and Politics</p>
                    <p class="mb-4">From our bespoke museums to the stories of our people, our region is the perfect place to engage and understand the historical and political complexities of our land.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Derry City & Strabane</h2>
	        </div>
            <guides-index :region="'derry-city-strabane'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=derry-city-strabane">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'derry-strabane'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'derry-strabane'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection