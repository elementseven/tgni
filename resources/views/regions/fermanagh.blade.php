@php
$page = 'Fermanagh & Omagh';
$pagetitle = "Fermanagh & Omagh | Regions | Northern Ireland Tour Guides";
$metadescription = "This hidden gem of Northern Ireland is a labyrinth of natural beauty and wonder.  Fermanagh and Omagh never disappoint.";
$pagetype = 'dark';
$pagename = 'fermanagh-omagh';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-fermanagh bg z-2">
    <span class="trans"></span>
    <div class="row">
        <div class="container container-wide mob-px-4">
            <div class="row">
              <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
                <div class="d-table w-100 h-100">
                  <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
                    <p class="region-top text-uppercase">Stairway to Heaven</p>
                    <p class="region-title">Fermanagh<br/>& Omagh</p>
                    <p>This hidden gem of Northern Ireland is a labyrinth of natural beauty and wonder.  Fermanagh and Omagh never disappoint.</p>
                    <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
            <div class="row py-5 mob-py-0">
                <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
                    <p class="mimic-h2 mb-2">Fermanagh & Omagh</p>
                    <p class="mb-4 mob-mb-3">Join local guides Richard Orr from Bestfast Tours and Conor Owens from Belfast Hidden Tours explore the Fermanagh and Omagh region. From the historic houses and castles to the small heritage village of Arney, this region is a fantastic way to discover all parts of local traditions and cultures.  We highly recommend getting on the boat around the only island town in Ireland. Get in touch with Richard, Conor or another local guide to plan your next trip to Fermanagh and Omagh</p>
                    <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
                </div>
                <div class="col-lg-7 text-center">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YcvQ-B5Ouas?rel=0" allowfullscreen></iframe>
                    </div>
                    <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/fermanagh/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/fermanagh/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/fermanagh/1.jpg" type="image/jpeg" alt="The Lakelands of Ireland - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">The Lakelands of Ireland</p>
                    <p class="mb-4">Enniskillen, Irelands only island town, is a perfect base to settle into the regions way of life.  Idyllic settings provide bliss for fishermen, boats, families and visitors alike.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">The Stairway to Heaven</p>
                    <p class="mb-4">For the adventurous, The Cuilcagh Boardwalk Trail is a rewarding and spectacular way to experience the local scenery and wildlife of Fermanagh & Omagh.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/fermanagh/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/fermanagh/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/fermanagh/2.jpg" type="image/jpeg" alt="The Stairway to Heaven - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">The Stairway to Heaven</p>
                    <p class="mb-4">For the adventurous, The Cuilcagh Boardwalk Trail is a rewarding and spectacular way to experience the local scenery and wildlife of Fermanagh & Omagh.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/fermanagh/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/fermanagh/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/fermanagh/3.jpg" type="image/jpeg" alt="USESCO Marble Arch Caves - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">UNESCO ‘Marble Arch Caves’</p>
                    <p class="mb-4">Travel through the caves and discover landscapes that were created over 340 million years ago.  This global geopark site is officially recognised by UNESCO.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">A land of many times</p>
                    <p class="mb-4">Fermanagh and Omagh region is a perfect place to delve into the complex histories of its people.  From ancient pagan sites, tales of the native farmers to the regions National Trust treasure houses.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/fermanagh/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/fermanagh/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/fermanagh/events/4.jpg" type="image/jpeg" alt="A land of many times - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">A land of many times</p>
                    <p class="mb-4">Fermanagh and Omagh region is a perfect place to delve into the complex histories of its people.  From ancient pagan sites, tales of the native farmers to the regions National Trust treasure houses.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
            <div class="container">
            <div class="row">
            <div class="col-12 mob-px-4 text-center text-lg-left">
                <h2 class="mob-mb-2 mb-4">Our Guides for Fermanagh & Omagh</h2>
            </div>
            <guides-index :region="'fermanagh-omagh'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=fermanagh-omagh">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
        </div>
      </div>
    </div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'fermanagh-omagh'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'fermanagh-omagh'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
    window.onload = (event) => {
    var w = parseInt($(window).innerWidth());
    var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
        if(w >= 767){
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top - 150
          }, 500);
        }else{
            $('html, body').animate({
            scrollTop: $("#scrollToGuides").offset().top + 0
          }, 500);
        }
    });
  };
</script>
@endsection