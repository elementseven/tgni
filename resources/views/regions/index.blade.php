@php
$page = 'Regions';
$pagetitle = "Regions | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'belfast-city';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <div class="container position-relative py-5 mob-pb-3 mt-5 mob-mt-0">
            <div class="row pt-5 mt-5">
                <div class="col-lg-8  position-relative z-2 text-center text-lg-left">
                    <h1 class="mb-4">Discover Our Regions</h1>
                    <p>Tour Guides NI members are treasured ambassadors of our people, places and products. Explore our council regions and be inspired and excited about your next trip to our shores with one of our many professional guides.</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
<desktop-regions class="d-none d-lg-block"></desktop-regions>
<mob-regions class="d-lg-none"></mob-regions>
<div class="container mt-5 mob-mt-0 pt-5 mob-px-4">
    <div class="row">
        <div class="col-lg-8 text-center text-lg-left">
            <h2 class="mb-2">Our Regions, Our Guides</h2>
            <p>Tour Guides NI has over 100 tour guides to make your trip a memorable experience throughout all our lands.</p>
            <p class="mb-4">Our guides have vast experience and knowledge in the tourism industry.  Get in touch with a guide today to learn more.</p>
            <a href="/guides">
                <button class="btn btn-primary" type="button">View guides</button>
            </a>
        </div>
    </div>
</div>
<seen-enough :link="'/guides'" :btntext="'Find a guide'" :img="'ards'" title="Ready to book?" text="Come and explore the beautiful regions of Northern Ireland with one of our talented tour guides."></seen-enough>
@endsection
@section('scripts')

@endsection