@php
$page = 'Lisburn & Castlereagh';
$pagetitle = "Lisburn & Castlereagh | Regions | Northern Ireland Tour Guides";
$metadescription = "Home to the historic Hillsborough Castle and many beautiful nature spots, this region is perfect for families, foodies and relaxing holidays.";
$pagetype = 'dark';
$pagename = 'lisburn-castlereagh';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-lisburn bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">IRISH LINEN CENTRE</p>
			        <p class="region-title">Lisburn & Castlereagh</p>
			        <p>Home to the historic Hillsborough Castle and many beautiful nature spots, this region is perfect for families, foodies and relaxing holidays.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Lisburn & Castlereagh</p>
		            <p class="mb-4 mob-mb-3">Join local guide Gerry McClory explore and discover the Lisburn and Castlereagh region. From the shops in Lisburn, food specialists in Moira to the historic Royal village of Hillsborough, this is a great region for a getaway in luxurious surroundings.  Numerous parks, farms and a top-class swimming complex make this an ideal place for families. Get in touch with Gerry or another local guide to plan your next trip to the Lisburn and Castlereagh region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rCYR9wUVEkU?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/lisburn/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/lisburn/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/lisburn/1.jpg" type="image/jpeg" alt="Historic Hillsborough - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Historic Hillsborough</p>
                    <p class="mb-4">As well as official home to the Queen, Hillsborough Castle and Gardens is one of the country’s top tourist destination. The picturesque village is home to traditional pubs, restaurants and shops.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Moira – A town of taste</p>
                    <p class="mb-4">If food and drink are one of your top ticks as a traveller, then Moira is the place for you. Tour Guides NI would recommend the Stillhouse and partaking in their lively Gin School.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/lisburn/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/lisburn/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/lisburn/2.jpg" type="image/jpeg" alt="Moira – A town of taste - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Moira – A town of taste</p>
                    <p class="mb-4">If food and drink are one of your top ticks as a traveller, then Moira is the place for you. Tour Guides NI would recommend the Stillhouse and partaking in their lively Gin School.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/lisburn/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/lisburn/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/lisburn/3.jpg" type="image/jpeg" alt="Irelands oldest independent brewery - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Irelands oldest independent brewery</p>
                    <p class="mb-4">Hilden Brewery in Lisburn is a superb stop to sample local beers and food.  Keep an eye on their events, especially their annual beer festival, who said you can’t organise a party in a brewery.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Home of Irish Linen</p>
                    <p class="mb-4">The Irish Linen Centre in Lisburn is the perfect place to learn about one of our iconic industries.  Get in touch with one of our guides on upcoming events and workshops.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/lisburn/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/lisburn/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/lisburn/4.jpg" type="image/jpeg" alt="Home of Irish Linen - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Home of Irish Linen</p>
                    <p class="mb-4">The Irish Linen Centre in Lisburn is the perfect place to learn about one of our iconic industries.  Get in touch with one of our guides on upcoming events and workshops.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Lisburn & Castlereagh</h2>
	        </div>
            <guides-index :region="'lisburn-castlereagh'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=lisburn-castlereagh">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'lisburn-castlereagh'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'lisburn-castlereagh'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection