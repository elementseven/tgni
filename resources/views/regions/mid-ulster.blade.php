@php
$page = 'Mid Ulster';
$pagetitle = "Mid Ulster | Regions | Northern Ireland Tour Guides";
$metadescription = "At the heart of Ulster, Mid-Ulster is home to our traditions, tales and some of the best tour offerings in the land.";
$pagetype = 'dark';
$pagename = 'mid-ulster';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-mid-ulster bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Seamus Heaney ‘Homeplace’</p>
			        <p class="region-title">Mid Ulster</p>
			        <p>At the heart of Ulster, Mid-Ulster is home to our traditions, tales and some of the best tour offerings in the land.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Mid Ulster</p>
		            <p class="mb-4 mob-mb-3">Join local guide Bronagh Mallon explore and discover the Mid-Ulster region. This magical area is not much of a hidden secret these days.  From storytelling at Friels Bar, sheepdog training with Jamesy, traditional cooking with Bronagh at her bakehouse, this is the region were traditions and passion are in abundance. Get in touch with Bronagh or another local guide to plan your next trip to the Mid-Ulster region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/92WUGlSMHUY?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/mid-ulster/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/mid-ulster/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/mid-ulster/1.jpg" type="image/jpeg" alt="Seamus Heaney Homeplace - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Seamus Heaney ‘Homeplace’</p>
                    <p class="mb-4">‘Homeplace’ in Bellaghy is the perfect homage to one of Irelands greatest writers.  Be truly inspired in this visitor attraction that won Tripadvisor Travellers Choice Award in 2020.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">The seat of Ulster</p>
                    <p class="mb-4">Mid-Ulster is a region awashed with myths and history.  Visit the Hill of O’Neill and feel the ancient history of the lands from its people.  Contact one of our guides to learn more.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/mid-ulster/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/mid-ulster/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/mid-ulster/2.jpg" type="image/jpeg" alt="The seat of Ulster - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">The seat of Ulster</p>
                    <p class="mb-4">Mid-Ulster is a region awashed with myths and history.  Visit the Hill of O’Neill and feel the ancient history of the lands from its people.  Contact one of our guides to learn more.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/mid-ulster/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/mid-ulster/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/mid-ulster/3.jpg" type="image/jpeg" alt="OM Dark Sky Park - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">OM Dark Sky Park</p>
                    <p class="mb-4">A one of its kind, where astronomy meets archelogy.  This is the first Dark Sky Park in Northern Ireland, find out from a local tour guide the best way and time to visit.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Experience Tradition</p>
                    <p class="mb-4">This region is one of a kind as it embraces and cherishes the old traditions and ways of its people.  From famine pots and soda bread to sheep-dog training and eel fisheries, time to taste our traditions.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/mid-ulster/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/mid-ulster/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/mid-ulster/4.jpg" type="image/jpeg" alt="Experience Tradition - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Experience Tradition</p>
                    <p class="mb-4">This region is one of a kind as it embraces and cherishes the old traditions and ways of its people.  From famine pots and soda bread to sheep-dog training and eel fisheries, time to taste our traditions.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Mid Ulster</h2>
	        </div>
            <guides-index :region="'mid-ulster'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=mid-ulster">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'mid-ulster'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'mid-ulster'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection