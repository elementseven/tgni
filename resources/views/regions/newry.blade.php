@php
$page = 'Newry, Mourne & Down';
$pagetitle = "Newry, Mourne & Down | Regions | Northern Ireland Tour Guides";
$metadescription = "St. Patrick's story, areas of natural beauty, world class food, drink and hospitality.  This region has it all, plus more.";
$pagetype = 'dark';
$pagename = 'newry-mourne-down';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-mournes bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">MOURNE MOUNTAINS</p>
			        <p class="region-title">Newry, Mourne <br/>& Down</p>
			        <p>St. Patrick's story, areas of natural beauty, world class food, drink and hospitality.  This region has it all, plus more.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Newry, Mourne & Down</p>
		            <p class="mb-4 mob-mb-3">Join local guide Anne Fearon from Gullion tours explore and discover the Newry, Mourne and Down region.  This region is historically known for its rugged landscapes and legendary storytelling traditions.  Anne visits graveyards, castles and even manages to get a special song sung in the iconic Area of Natural Beauty, the Ring of Gullion. Get in touch with Anne or another local guide to plan your next trip to the Newry, Mourne and Down region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/bhHuvgc8PA8?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/newry/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newry/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newry/1.jpg" type="image/jpeg" alt="Land of magic and fantasy - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Land of magic and fantasy</p>
                    <p class="mb-4">The makers of Game of Thrones and the dreamer of Narnia, CS Lewis, both know an enchanted magical place when they find one.  Follow in their footsteps, making your own adventures.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Mourne Mountains</p>
                    <p class="mb-4">Contact one of our professional guides that can make even the most mountainous looking trek a walk in the park for you and your guest in the majestic Mourne Mountains</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/newry/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newry/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newry/2.jpg" type="image/jpeg" alt="Mourne Mountains, country down - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Mourne Mountains</p>
                    <p class="mb-4">Contact one of our professional guides that can make even the most mountainous looking trek a walk in the park for you and your guest in the majestic Mourne Mountains.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/newry/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newry/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newry/3.jpg" type="image/jpeg" alt="Ring of Gullion - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Ring of Gullion</p>
                    <p class="mb-4">Within this Area of Natural Beauty lies some of the best kept secrets this side of the Irish Sea.  Let one of our local guides create a magical and memorable day.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Christian Heritage</p>
                    <p class="mb-4">Discover how all of Irelands 3 saints lie within the same grave while following the steps and stories that have helped Christianity thrive in Ireland and throughout the world.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/newry/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newry/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newry/4.jpg" type="image/jpeg" alt="Christian Heritage - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Christian Heritage</p>
                    <p class="mb-4">Discover how all of Irelands 3 saints lie within the same grave while following the steps and stories that have helped Christianity thrive in Ireland and throughout the world.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Newry, Mourne & Down</h2>
	        </div>
            <guides-index :region="'newry-mourne-down'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=newry-mourne-down">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'newry-mourne-down'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'newry-mourne-down'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection