@php
$page = 'Antrim & Newtownabbey';
$pagetitle = "Antrim & Newtownabbey | Regions | Northern Ireland Tour Guides";
$metadescription = "Historic Antrim and surrounding area is a region steeped with local lore, mythology, battles and tales of rebellion. Book a guided tour and see for yourself.";
$pagetype = 'dark';
$pagename = 'antrim-newtownabbey';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative region-header regions-newtownabbey bg z-2">
	<span class="trans"></span>
	<div class="row">
		<div class="container container-wide mob-px-4">
			<div class="row">
			  <div class="col-lg-6 px-5 mob-px-3 full-height position-relative z-2 text-white">
			    <div class="d-table w-100 h-100">
			      <div class="d-table-cell w-100 h-100 align-middle text-center text-lg-left">
			        <p class="region-top text-uppercase">Nature & Culture</p>
			        <p class="region-title">Antrim <br/>& Newtownabbey</p>
			        <p>Historic Antrim and surrounding area is a region steeped with local lore, mythology, battles and tales of rebellion. Book a guided tour and see for yourself.</p>
			        <button class="btn btn-primary scrollTo" type="button">Find a Guide</button>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
  </div>
  <scroll-indicator></scroll-indicator>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
	<img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
	<div class="row">
		<div class="container mt-5 pt-5 mob-mt-0 mob-pt-0 position-relative z-2">
		    <div class="row py-5 mob-py-0">
		        <div class="col-lg-5 text-center text-lg-left mob-px-4 mob-mt-5 mob-mb-4">
		            <p class="mimic-h2 mb-2">Antrim & Newtownabbey</p>
		            <p class="mb-4 mob-mb-3">Join local guide Donal Kelly from Belfast Mic Tours explore and discover the Antrim and Newtownabbey region. Located in close proximity to capital city Belfast, this region gives a great insight to the historical treasures of our land, from the shores of Lough Neagh to fine examples of round towers, join guides such as Donal to bring these regions and stories to life. Get in touch with Donal or another local guide to plan your next trip to the Antrim and Newtownabbey region.</p>
		            <button class="btn btn-primary scrollTo d-none d-lg-block" type="button">Find a Guide</button>
		        </div>
		        <div class="col-lg-7 text-center">
		            <div class="embed-responsive embed-responsive-16by9">
		              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6RrCoCFCvwc?rel=0" allowfullscreen></iframe>
		            </div>
		            <button class="btn btn-primary scrollTo mt-4 d-lg-none" type="button">Find a Guide</button>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div class="container-fluid py-5 position-relative z-2">
    <div class="row py-5 mb-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/newtownabbey/1.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newtownabbey/1.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newtownabbey/1.jpg" type="image/jpeg" alt="Historic Castles & Gardens - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2">Historic Castles & Gardens</p>
                    <p class="mb-4">Antrim and Newtownabbey region has beautiful places for visitors of all ages to explore, Shane’s Castle and Antrim Castle Gardens are two of our top tips to visit.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Ancient sites and myths</p>
                    <p class="mb-4">Our region is home to many local myths and legends.  Book a local guide to showcase the area and hear how the ‘Witch’s Stone’ got its name at the glorious Antrim Round Tower.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/newtownabbey/2.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newtownabbey/2.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newtownabbey/2.jpg" type="image/jpeg" alt="Ancient sites and myths - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Ancient sites and myths</p>
                    <p class="mb-4">Our region is home to many local myths and legends.  Book a local guide to showcase the area and hear how the ‘Witch’s Stone’ got its name at the glorious Antrim Round Tower.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 pl-0 mob-px-4">
            <div class="left-img">
                <picture>
                    <source srcset="/img/regions/newtownabbey/3.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newtownabbey/3.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newtownabbey/3.jpg" type="image/jpeg" alt="Take to our shore - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 pl-5 mob-px-3 mob-mt-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left mob-px-4">
                    <p class="mimic-h2 mb-2 mob-mt-3">Take to our shore</p>
                    <p class="mb-4">Lough Neagh is the largest freshwater lake in UK and Ireland.  Antrim is a perfect way to take in the life and people of our local waterways, maybe even taste our famous eels.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row py-5 my-5 mob-my-0">
        <div class="col-lg-6 col-xl-4 offset-xl-2 pr-5 d-none d-lg-block">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center mob-px-4">
                    <p class="mimic-h2 mb-2">Nature tours</p>
                    <p class="mb-4">One of the secrets to our successes, is the nature all around us.  Contact one of our professional guides and delve into the magical forests, waterways, lakes and raths of our lands.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6 pr-0 mob-px-4">
            <div class="right-img">
                <picture>
                    <source srcset="/img/regions/newtownabbey/4.webp" type="image/webp"/> 
                    <source srcset="/img/regions/newtownabbey/4.jpg" type="image/jpeg"/> 
                    <img src="/img/regions/newtownabbey/events/4.jpg" type="image/jpeg" alt="Nature tours - TGNI" class="w-100" />
                </picture>
            </div>
        </div>
        <div class="col-12 d-lg-none px-4 mt-4">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-lg-right text-center">
                    <p class="mimic-h2 mb-2">Nature tours</p>
                    <p class="mb-4">One of the secrets to our successes, is the nature all around us.  Contact one of our professional guides and delve into the magical forests, waterways, lakes and raths of our lands.</p>
                    <button class="btn btn-primary scrollTo">Book a guide</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="scrollToGuides" class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
			<div class="container">
		    <div class="row">
	        <div class="col-12 mob-px-4 text-center text-lg-left">
	            <h2 class="mob-mb-2 mb-4">Our Guides for Antrim & Newtownabbey</h2>
	        </div>
            <guides-index :region="'antrim-newtownabbey'" :showregion="false" :type="'*'" :lang="'*'" :title="0" :limit="4" :search="''"></guides-index>
            <div class="col-12 mt-4 text-center">
                <a href="/guides?region=antrim-newtownabbey">
                    <button class="btn btn-primary" type="button">View more guides</button>
                </a>
            </div>
	    </div>
	  </div>
	</div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-mb-0">
    <div class="row">
        <div class="col-12 text-center text-lg-left px-4">
            <p class="mimic-h2 mb-5 mob-mb-4">Discover Other Regions</p>
        </div>
        <desktop-regions class="d-none d-lg-block" :exclude="'antrim-newtownabbey'"></desktop-regions>
        <mob-regions class="d-lg-none" :exclude="'antrim-newtownabbey'"></mob-regions>
    </div>
</div>
@endsection
@section('scripts')
<script>
	window.onload = (event) => {
  	var w = parseInt($(window).innerWidth());
  	var h = parseInt($(window).innerHeight());
    $(".scrollTo").click(function (){
    	if(w >= 767){
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top - 150
	      }, 500);
    	}else{
    		$('html, body').animate({
	        scrollTop: $("#scrollToGuides").offset().top + 0
	      }, 500);
    	}
    });
  };
</script>
@endsection