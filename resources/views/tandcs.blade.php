@php
$page = 'Terms & Conditions';
$pagetitle = "Terms & Conditions | Northern Ireland Tour Guides";
$metadescription = "The capital of Northern Ireland features many years of history as well as world famous attractions.";
$pagetype = 'light';
$pagename = 'tandcs';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 mt-5 mob-mt-0">
	<div class="row pt-5 mt-5">
		<div class="col-lg-8  position-relative z-2">
			<h1 class="mb-4">Terms & Conditions</h1>
			<p class="mb-4">Last updated: 21/08/2021</p>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container pb-5 position-relative z-2">
	<div class="row pb-5 mb-5">
		<div class="col-12">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper erat ut velit congue, in ultricies urna auctor. Vestibulum dictum metus porta, varius mi et.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
		</div>
	</div>
</div>

@endsection
@section('scripts')

@endsection