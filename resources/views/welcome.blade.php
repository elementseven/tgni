@php
$page = 'Homepage';
$pagetitle = "Northern Ireland Tour Guides - Let Us Show You Around";
$metadescription = "We have a broad range of Qualified Northern Ireland Tour Guides ranging from Site Guides to City Guides and all Ireland Guides.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://tourguidesni.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<home-header></home-header>
@endsection
@section('content')
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-1.svg" alt="TGNI - hexagons graphic 1" class="hexagons-1"/>
    <div class="row">
        <guides-index :region="'*'" :type="'*'" :lang="'*'" :title="1" :search="''" :limit="16" :showregion="true" :lm="false"></guides-index>
        <div class="col-12 mt-4 text-center">
            <a href="/guides">
                <button class="btn btn-primary d-inline-block">View more guides</button>
            </a>
        </div>
    </div>
</div>
<div class="container mt-5 pt-5 mob-px-4 position-relative z-2">
    <div class="row">
        <div class="col-lg-8 text-center text-lg-left">
            <h2 class="mb-2">Tour Guides NI</h2>
            <p class="mb-4">We are a collection of professional tour guides from Northern Ireland and beyond. Get in touch with our members and association to start connecting with best ambassadors, storytellers and tour guides on offer.</p>
            <a href="/regions">
                <button class="btn btn-primary" type="button">View Regions</button>
            </a>
        </div>
    </div>
</div>
<div class="container-fluid py-5 position-relative">
    <div class="row py-5 mb-5 mob-mb-0">
        <div class="col-lg-6 pl-0 mob-px-4 mob-mb-4">
            <div class="left-img">
                <picture>
                    <source data-srcset="/img/regions/belfast/titanic-museum.webp" type="image/webp"/> 
                    <source data-srcset="/img/regions/belfast/titanic-museum.jpg" type="image/jpeg"/> 
                    <img data-src="/img/regions/belfast/events/titanic-museum.jpg" type="image/jpeg" alt="Belfast's Titanic Museum - TGNI" class="lazy w-100" />
                </picture>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 mob-px-4 pl-5 mob-px-3">
            <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100 text-center text-lg-left">
                    <p class="mimic-h2 text-capitalize mb-2">Explore our<br/>magical places</p>
                    <p class="mb-4">Our regions are blessed with iconic venues, magical stories and passionate people. Get in touch with a guide that will ensure you make unforgettable memories and connections.</p>
                    <a href="/guides">
                        <button class="btn btn-primary">Find a guide</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid position-relative">
    <img src="/img/graphics/hexagons-2.svg" alt="TGNI - hexagons graphic 2" class="hexagons-2"/>
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3 mob-px-4">
                    <p class="mimic-h2 mb-4 mob-mb-3 text-center text-lg-left">Discover Our Regions</p> 
                </div>
            </div>
        </div>
    </div>
</div>
<desktop-regions class="d-none d-lg-block"></desktop-regions>
<mob-regions class="d-lg-none"></mob-regions>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <a href="/regions" class="text-dark mb-4">
                    <button type="button" class="btn btn-primary mt-3 mx-auto">View All Regions</button>
                </a>
            </div>
        </div>
    </div>
</div>
<seen-enough :link="'/guides'" :btntext="'Book a guide'" :img="'belfast'" title="Ready to book?" text="Come and explore the beautiful regions of Northern Ireland with one of our talented tour guides."></seen-enough>@endsection
@section('scripts')

@endsection