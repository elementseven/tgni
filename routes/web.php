<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/regions', [PageController::class, 'regions'])->name('regions');
Route::get('/regions/causeway-coast-glens', [PageController::class, 'regionsCauseway'])->name('regionsCauseway');
Route::get('/regions/belfast-city', [PageController::class, 'regionsBelfastCity'])->name('regionsBelfastCity');
Route::get('/regions/fermanagh-omagh', [PageController::class, 'regionsFermanagh'])->name('regionsFermanagh');
Route::get('/regions/newry-mourne-down', [PageController::class, 'regionsNewry'])->name('regionsNewry');
Route::get('/regions/derry-strabane', [PageController::class, 'regionsDerry'])->name('regionsDerry');
Route::get('/regions/antrim-newtownabbey', [PageController::class, 'regionsNewtownabbey'])->name('regionsNewtownabbey');
Route::get('/regions/mid-east-antrim', [PageController::class, 'regionsAntrim'])->name('regionsAntrim');
Route::get('/regions/ards-north-down', [PageController::class, 'regionsArds'])->name('regionsArds');
Route::get('/regions/lisburn-castlereagh', [PageController::class, 'regionsLisburn'])->name('regionsLisburn');
Route::get('/regions/armagh-banbridge-craigavon', [PageController::class, 'regionsArmagh'])->name('regionsArmagh');
Route::get('/regions/mid-ulster', [PageController::class, 'regionsMidUlster'])->name('regionsMidUlster');

Route::get('/guides', [GuideController::class, 'index'])->name('guides');
Route::get('/guide/{profile}/{slug}', [GuideController::class, 'show'])->name('guidesShow');
Route::get('/get/guides', [GuideController::class, 'get'])->name('guidesGet');
Route::get('/get/filter-options', [GuideController::class, 'getFilterOptions'])->name('getFilterOptions');
Route::get('/get/guide-images/{profile}', [GuideController::class, 'getGuideImages'])->name('getGuideImages');

Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/join', [PageController::class, 'join'])->name('join');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacyPolicy');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');

// Tour Guides
Route::get('/tour-guide-login', [PageController::class, 'tourGuideLogin'])->name('tourGuideLogin');

Route::post('/send-message', [SendMail::class, 'enquiry'])->name('sendMessage');
Route::post('/send-join-form', [SendMail::class, 'sendJoinForm'])->name('sendJoinForm');
Route::post('/send-booking-request', [SendMail::class, 'sendBookingRequest'])->name('sendBookingRequest');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailingList');
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('news.index');
Route::get('/news/get', [HomeController::class, 'get'])->name('news.get');
Route::get('/news/{date}/{slug}', [HomeController::class, 'show'])->name('news.show');

Route::get('/logout', function () {
    Auth::logout();
    return redirect()->to('/login');
});